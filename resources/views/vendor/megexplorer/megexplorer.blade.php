<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('vendor/megexplorer/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/megexplorer/css/fa/css/all.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('vendor/megexplorer/js/dropzone/dropzone.css') }}">
    @stack('styles')
    {{--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">--}}
</head>
<body>
<div id="app">
    @yield('content')
</div>

<!-- Scripts -->
<script type="text/javascript" src="{{ asset('vendor/megexplorer/js/dropzone/dropzone.js') }}"></script>
<script src="{{ asset('vendor/megexplorer/js/app.js') }}"></script>
@stack('scripts')
</body>
</html>
