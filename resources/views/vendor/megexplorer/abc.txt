<div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        You are logged in!

                        <button id="a1" onclick="openSelect(event)">Select</button>
                        <button id="a2" onclick="openSelect(event)">Select</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        window.addEventListener("fileSelect", function(e){
            console.log(e);
        });

        function openSelect(e) {

            var default_params = {
                mode: "select_file", //select_disk, select_folder
                //container: "body",
                file_activation_mode: "preselect",
                allow_create_folder: true,
            };

            var wind = popupCenter(window.location.origin + "/megexplorer" + "?data=" + window.btoa(JSON.stringify(default_params)), "File Select", 600, 600);
            console.log(e);
            wind.onunload = function () {
                console.log(e.target.id, wind.m.params);
            }
        }

        function popupCenter(url, title, w, h) {

            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : window.screenX;
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : window.screenY;
            var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
            var left = ((width / 2) - (w / 2)) + dualScreenLeft;
            var top = ((height / 2) - (h / 2)) + dualScreenTop;
            var newWindow = window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

            if (window.focus) {
                newWindow.focus();
            }

            return newWindow;

        }
    </script>