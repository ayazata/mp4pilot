@extends('Megexplorer::megexplorer')

@section('content')
    <div class="dz hidden">
        {{--            <div id="dropzone">
                        <form action="/upload-test" class="dropzone">
                            <input type="hidden" name="active_disk_key" value="0">
                            <input type="hidden" name="active_folder_id" value="0">
                            {{ csrf_field() }}
                        </form>
                    </div>--}}
    </div>
    </div>
    <div class="container-fluid">
        <div class="row-fluid megexplorer-target">

        </div>
    </div>

@endsection


@push('styles')

    <style type="text/css">

        .megexplorer-pane {
            border: 1px solid #ddd;
            margin: 0px 0px 5px 0px;
            clear: both;
            overflow: hidden;
        }

        .megexplorer-toolbar {
            margin-top: -1px;
            margin-left: -1px;
        }

        .megexplorer-toolbar .btn {
            border-radius: 0;
            border-top: none;
        }

        .megexplorer-lv {
            margin: 10px;
            min-height: 200px;
            height: auto;
            overflow: hidden;
        }

        .megexplorer-bc {
            margin-top: 10px;
            margin-bottom: -1px;
            border: 1px solid #ddd;
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
        }

        .megexplorer-wrapper .dropzone {
            border-width: 1px 0 1px 0;
            border-color: #ddd;
            margin-top: -1px;
        }

        .megexplorer-upload-file.on {
            background-color: #0f74a8;
            color: #fff;
        }

        .megexplorer-list-item.highlighted {
            border: 2px solid #0a5ae5;
        }

        .mdisk, .mfolder, .mfile {
            position: relative;
            border: 2px solid transparent;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            color: #0a5ae5;
            padding: 5px;
            text-align: center;
            display: inline-block;
            float: left;
            width: 110px;
        }

        .mdisk > .fas, .mfolder > .fas, .mfile > .fas {
            font-size: 96px;
            margin: 5px;
        }

        .mfile > div.img-holder {
            margin: 5px;
            position: relative;
            box-sizing: border-box;
            width: 86px;
            height: 86px;
            overflow: hidden;
        }

        .mfile > div.img-holder > img {
            display: block;
            position: relative;
            margin: 0px auto;
            max-width: 100%;
            max-height: 96px;
            height: 100%;
        }

        .mdisk > .disk_name, .mfolder > .folder_name, .mfile > .file_name {
            display: block;
            margin: 3px 0;
            color: #000;
            white-space: nowrap;
            text-overflow: ellipsis;
            overflow: hidden;
        }
    </style>

@endpush


@push('scripts')
    <script type="text/javascript">
        deparam = function (querystring) {
            // remove any preceding url and split
            querystring = querystring.substring(querystring.indexOf('?') + 1).split('&');
            var params = {}, pair, d = decodeURIComponent;
            // march and parse
            for (var i = querystring.length - 1; i >= 0; i--) {
                pair = querystring[i].split('=');
                params[d(pair[0])] = d(pair[1] || '');
            }

            return params;
        };

        /*function selectFile(disk, folder, file) {
            var is_ckeditor = deparam(window.location.search)['CKEditor'];
            var url = "{!! url(route('megexplorer.dlbase')) !!}/" + disk + "/" + folder + "/" + file;
            if (is_ckeditor) {
                if (window.opener) {
                    window.opener.CKEDITOR.tools.callFunction(deparam(window.location.search)['CKEditorFuncNum'], url);
                } else {
                    parent.CKEDITOR.tools.callFunction(deparam(window.location.search)['CKEditorFuncNum'], url);
                    parent.CKEDITOR.tools.callFunction(deparam(window.location.search)['CKEditorCleanUpFuncNum']);
                }
            } else {
                window.opener.SetUrl(url, "", file);
            }
        }*/
    </script>
    <script type="text/javascript">


        //TODO: klasör oluşturma
        const Megexplorer = function ($params) {
            function getDownloadUrlFromModel(model) {
                var url = model.active_file.name;
                $.each(m.ui.bc_items, function (ind, el) {
                    if (el.type == "folder") {
                        url = el.name + "/" + url
                    }
                    else if (el.type == "disk") {
                        url = el.root + url
                    }
                });
                console.log(url);
                return url;
            }

            const callbacks = {
                ckeditor: function (model) {
                    var url = getDownloadUrlFromModel(model);
                    if (window.opener) {
                        window.opener.CKEDITOR.tools.callFunction(deparam(window.location.search)['CKEditorFuncNum'], url);
                    } else {
                        parent.CKEDITOR.tools.callFunction(deparam(window.location.search)['CKEditorFuncNum'], url);
                        parent.CKEDITOR.tools.callFunction(deparam(window.location.search)['CKEditorCleanUpFuncNum']);
                    }
                    window.close();
                },
                native: function (model) {
                    var url = getDownloadUrlFromModel(model);
                    window.opener.SetUrl(url, "", model.active_file.id);
                    window.close();
                }
            }

            const default_params = {
                mode: "select_file", //select_disk, select_folder
                filter_presets:
                    [
                        {
                            "key": "all",
                            "title": "Tüm Dosyalar",
                            "regex": /.*/
                        },
                        {
                            "key": "images",
                            "title": "Görseller",
                            "regex": /^([a-zA-Z0-9._ıİşŞçÇüÜğĞöÖ\s-]*)?([.](jpg|jpeg|png|gif|bmp|))+$/
                        },
                        {
                            "key": "documents",
                            "title": "Belgeler",
                            "regex": /^([a-zA-Z0-9._ıİşŞçÇüÜğĞöÖ\s-]*)?([.](pdf|doc|docx|xls|xlsx|xml|xmlx|ppt|pptx|pps|ppsx|odt|ods|odp|odg|epub|htm|html|mdb|rar|zip|7z|tar\.gz))+$/
                        },
                        {
                            "key": "multimedia",
                            "title": "Çoklu Ortam",
                            "regex": /^([a-zA-Z0-9._ıİşŞçÇüÜğĞöÖ\s-]*)?([.](mp3|mp4|avi|webm|mov|wav|mp2|flv|wmv|mpg))+$/
                        }
                    ],
                selectable_filter_presets: [ //in appearance order
                    "all",
                    "images",
                    "documents",
                    "multimedia"
                ],
                filter_preset: "all",
                container: ".megexplorer-target",
                active_disk: {diskkey: null},
                active_folder: {id: null},
                active_file: null,
                thumbnails_diskkey: null,

                file_activation_mode: "preselect",
                allow_create_folder: true,
                key: null,
                last_nav_data: [null, null],
                callback: "native"
            };

            const _token = document.getElementsByName("csrf-token")[0].value;//$('meta[name=csrf-token]').attr('content');

            this.ui = {};


            // merge user params with default ones and thus be sure all keys will allways be current;
            this.params = $.extend(default_params, $params);
            this.overlay = '<div style="display:none; position: absolute;width:100vw; height:100vh; background:rgba(0,0,0,0.2);"></div>';
            this.ui.diskcontext = '<div id="context_disk">Test</div>';
            this.ui.diskmodel = '<a class="megexplorer-list-item mdisk" href="#" data-disk_key="" data-contextmenu="context_disk"><i class="fas fa-compact-disc fa-4x"></i><span class="disk_name"></span></a>';
            this.ui.foldermodel = '<a class="megexplorer-list-item mfolder" href="#" data-disk_key="" data-folder_id=""><i class="fas fa-folder fa-4x"></i><span class="folder_name"></span></a>';
            this.ui.filemodel = '<a class="megexplorer-list-item mfile" href="#" data-disk_key="" data-folder_id="" data-file_id=""><i class="fas fa-file fa-4x"></i><div class="img-holder"><img src="https://picsum.photos/40/56?image=0"/></div><span class="file_name"></span></a>';
            this.ui.dropzonemodel = '<div id="dropzone" style="display:none;"><form action="/upload-test" class="dropzone"><input type="hidden" name="active_disk_key" value="0"><input type="hidden" name="active_folder_id" value="0">{{ csrf_field() }}</form></div>';
            this.ui.wrapper = "";
            this.ui.toolbar = "";
            this.ui.pane = "";
            this.ui.listview = "";
            this.ui.statusbar = "";
            this.ui.breadcrumb = "";
            this.ui.createfolderbtn = "";
            this.ui.uploadbtn = "";
            this.ui.goupbtn = "";
            this.ui.selectdiskbtn = "";
            this.ui.selectfolderbtn = "";
            this.ui.selectfilebtn = "";
            this.ui.deletebtn = "";
            this.ui.renamebtn = "";
            this.ui.bc_items = [];
            this.ui.filtertoolbar = '';


            /*<label class="btn btn-primary active"> <input type="radio" name="options" value="red" autocomplete="off" checked> Red </label>
          <label class="btn btn-primary">
            <input type="radio" name="options" value="orange" autocomplete="off"> Orange
            </label>
            <label class="btn btn-primary">
            <input type="radio" name="options" value="yellow" autocomplete="off"> Yellow
            </label>*/


            const megexplorer = this;

            //////-------------------------------------------------------------
            this.init = function ($container) {
                $container = megexplorer.params.container;
                megexplorer.params.key = this._makeid();
                // remove previous dom
                $(".megexplorer-wrapper", $container).remove();

                megexplorer.ui.wrapper = $('<div class="megexplorer-wrapper megexplorer-' + megexplorer.params.key + '"></div>');
                megexplorer.ui.dropzone = $(megexplorer.ui.dropzonemodel);
                megexplorer.ui.breadcrumb = $('<ul class="megexplorer-bc breadcrumb"></ul>');
                megexplorer.ui.toolbar = $('<div class="megexplorer-toolbar btn-toolbar" role="group" aria-label="..."></div>');
                megexplorer.ui.pane = $('<div class="megexplorer-pane"></div>');
                megexplorer.ui.listview = $('<div class="megexplorer-lv"></div>');
                megexplorer.ui.statusbar = $('<div class="megexplorer-status-bar"></div>');
                megexplorer.ui.createfolderbtn = $('<button class="btn btn-sm btn-primary megexplorer-create-folder"><i class="far fa-plus-square"></i> Dizin Oluştur</button>');
                megexplorer.ui.uploadbtn = $('<button class="btn btn-sm btn-primary megexplorer-upload-file off" data-disk-key="" data-folder-id=""><i class="fas fa-upload"></i> Dosya Yükle</button>');
                megexplorer.ui.goupbtn = $('<button class="btn btn-sm btn-primary megexplorer-go-up"><i class="fas fa-level-up-alt"></i> Yukarı</button>');
                megexplorer.ui.selectdiskbtn = $('<button class="btn btn-primary megexplorer-select-disk hidden pull-right" disabled><i class="fas fa-check-double"></i> Disk Seç</button>');
                megexplorer.ui.selectfolderbtn = $('<button class="btn btn-primary megexplorer-select-folder hidden pull-right" disabled><i class="fas fa-check-double"></i> Dizin Seç</button>');
                megexplorer.ui.selectfilebtn = $('<button class="btn btn-primary megexplorer-select-file hidden pull-right" disabled><i class="fas fa-check-double"></i> Dosya Seç</button>');
                megexplorer.ui.deletebtn = $('<button class="btn btn-sm btn-danger  megexplorer-delete pull-right" data-folder-id="" data-file-id="" disabled="disabled"><i class="fas fa-trash-alt"></i> Sil</button>');
                megexplorer.ui.renamebtn = $('<button class="btn btn-sm btn-warning  megexplorer-rename pull-right" data-folder-id="" data-file-id="" disabled="disabled"><i class="fas fa-pen-alt"></i> Yeniden Adlandır</button>');
                megexplorer.ui.filtertoolbar = $('<div class="btn-group megexplorer-filters-toolbar pull-left" data-toggle="buttons"></div>');

                // also embed initial params into the wrapper
                megexplorer.ui.wrapper.data('params', megexplorer.params);

                if (megexplorer.params['mode'] === undefined) {
                    megexplorer.params.mode = "select_file";
                }


                switch (megexplorer.params.mode) {
                    case "select_file":
                        megexplorer.navigate(megexplorer.params.active_disk.diskkey, megexplorer.params.active_folder.id);
                        break;
                    case "select_folder":
                        break;
                    case "select_disk":
                        break;
                    default:
                        break;
                }


                //this.steady();

                // create & tie new dom
                $($container).append(
                    $(megexplorer.ui.wrapper)
                        .append(megexplorer.ui.breadcrumb)
                        .append(
                            $(megexplorer.ui.pane).append(megexplorer.ui.toolbar).append(megexplorer.ui.dropzone).append(megexplorer.ui.listview)
                        ).append(megexplorer.ui.statusbar));

                megexplorer.Dropzone = new Dropzone("div#dropzone>form", {url: "/megexplorer/upload2"});
                megexplorer.Dropzone.on('complete', function () {
                    megexplorer.refresh_nav();
                });
                megexplorer.Dropzone.on('drag', function () {
                    console.log('drag');
                });
                /*megexplorer.Dropzone.on('dragenter',function(){console.log('dragenter');});
                megexplorer.Dropzone.on('dragstart',function(){console.log('dragstart');});
                megexplorer.Dropzone.on('dragleave',function(){console.log('dragleave');});*/

                //this.overlay = $(document).append(this.overlay);
                this.populateHandlers();
            };

            //////-------------------------------------------------------------

            this.getExtension = function (filename) {
                return filename.split('.').pop();
            };

            //////-------------------------------------------------------------

            this.navigate = function (diskkey, folder_id) {
                //console.log('nav data: ' + diskkey + " => " + folder_id);
                let items = megexplorer._getItems(diskkey, folder_id);
                megexplorer.last_nav_data = [diskkey, folder_id];
                megexplorer.ui.bc_items = items.breadcrumb;
                megexplorer._listItems(items.items, null);

                megexplorer.steady();
            };

            this.deneme = function () {
                $(megexplorer.Dropzone.element)
                    .css('display', "block")
                    .css("position", "fixed")
                    .css("width", window.innerWidth + "px")
                    .css("height", window.innerHeight + "px")
                    .css("z-index", 1000)
                    .css("top", 0)
                    .css("left", 0);
            };

            this.refresh_nav = function () {
                megexplorer.navigate(megexplorer.last_nav_data[0], megexplorer.last_nav_data[1]);
            };

            //////-------------------------------------------------------------

            this._getItems = function (diskkey, folder_id, file_id) {

                let url = '{!! url(route('megexplorer.index')) !!}';

                if (diskkey) {
                    url += '/' + diskkey;
                }
                if (folder_id) {
                    url += '/' + folder_id;
                }
                if (file_id) {
                    url += '/' + file_id;
                }

                let params = megexplorer.params;
                let result = megexplorer.xhrJson(url, params);
                //console.log(result.breadcrumb);

                if (result.nav_disk !== undefined) {
                    megexplorer.params.active_disk = result.nav_disk;
                }
                if (result.nav_folder !== undefined) {
                    megexplorer.params.active_folder = result.nav_folder;
                }

                if (result.nav_file !== undefined) {
                    megexplorer.params.active_file = result.nav_file;
                }
                return result;

            };

            /*this._selectItems = function (file_id) {
                let url = '{!! url(route('megexplorer.select',0)) !!}/0/' + file_id;

                let params = megexplorer.params;
                let result = megexplorer.xhrJson(url, params);
                console.log(result);
                if (result.nav_disk !== undefined) {
                    megexplorer.params.active_disk = result.nav_disk;
                }
                if (result.nav_folder !== undefined) {
                    megexplorer.params.active_folder = result.nav_folder;
                }
                if (result.nav_file !== undefined) {
                    megexplorer.params.active_file = result.nav_file;
                }

                return result;

            };*/

            //////-------------------------------------------------------------

            this._createFolder = function ($folder_name) {

                let url = '{!! url(route('megexplorer.root')) !!}' + '/create-folder/' + megexplorer.params.active_disk.diskkey;

                if (megexplorer.params.active_folder) {
                    url += '/' + megexplorer.params.active_folder.id;
                }

                let params = megexplorer.params;
                params.create_folder_name = $folder_name;
                let result = megexplorer.xhrJson(url, params);

                if (result.status) {
                    alert('Başarıyla oluşturuldu');
                    megexplorer.refresh_nav();
                } else {
                    let message = 'Oluşturulmadı';
                    if (result.errors.length == 1) {
                        message = ">>" + result.errors[0];
                    } else if (result.errors.length > 1) {
                        result.errors[0] = ">>" + result.errors[0];
                        message = result.errors.concat("\n>>");
                    }

                    alert(message);
                }

            };

            //////-------------------------------------------------------------

            this._removeItem = function (diskkey, folderid, fileid) {

                var url = '{!! url(route('megexplorer.root')) !!}';
                var cls_of_item = "not_defined_yet";
                folderid = folderid ? folderid : 0;

                if (fileid) {
                    url += '/remove/' + diskkey + '/' + folderid + '/' + fileid;
                    //cls_of_item = "mfile";
                } else {
                    url += '/remove/' + diskkey + '/' + folderid;
                    //cls_of_item = "mfolder";
                }

                let remove = megexplorer.xhrJson(url);

                return remove;


            };  //////-------------------------------------------------------------

            this._renameItem = function (newName, diskkey, folderid, fileid) {

                var url = '{!! url(route('megexplorer.root')) !!}';
                var cls_of_item = "not_defined_yet";
                folderid = folderid ? folderid : 0;

                url += '/rename/' + newName + '/' + diskkey + '/' + folderid;

                if (fileid) {
                    url += '/' + fileid;
                }

                let rename = megexplorer.xhrJson(url);

                return rename;


            };

            //////-------------------------------------------------------------

            this.applyFilter = function () {
                var $preset = megexplorer.getSelectedFilterPreset();
                //var $file = $(this);
                var $name = null;
                $('.mfile', megexplorer.ui.listview).each(function (ind, el) {
                    console.log($preset.regex);
                    $name = $(el).data("filemodel").name;
                    console.log($name);
                    //$(el).removeClass('hidden')
                    if (!$preset.regex.test($name)) {
                        $(el).addClass('hidden');
                    } else if ($(el).hasClass('hidden')) {
                        $(el).removeClass('hidden')
                    }

                });

            };

            //////-------------------------------------------------------------

            this.buildUrl = function ($action, $diskkey, $folder, $file) {

                //$action = $action ?? "index";

            };

            //////-------------------------------------------------------------

            this._prepareBreadcrumb = function () {
                $(megexplorer.ui.breadcrumb).html("");

                let bcitems = [];
                var is_first = true;

                var licontent = "";
                $.each(megexplorer.ui.bc_items, function (ind, el) {
                    let model = $("<li></li>");
                    if (is_first) {
                        $(model).addClass("active");
                        licontent = el.name;
                    } else {
                        licontent = '<a href="javascript:void(0);">' + el.name + '</a>'
                    }

                    bcitems.push($(model).append(licontent));
                    is_first = false;
                });

                bcitems.push($('<li><a href="#"><i class="fas fa-home"></i></a></li>'));
                bcitems.reverse();
                //bcitems[bcitems.length - 1].addClass("active");
                $.each(bcitems, function (ind, el) {
                    $(megexplorer.ui.breadcrumb).append(el[0])
                });
                /*if (megexplorer.params.active_disk) {
                    bcitems.push($('<li><a href="#">' + megexplorer.params.active_disk.name + '</a></li>'));
                    if (megexplorer.params.active_folder) {
                        bcitems.push($('<li><a href="#">' + megexplorer.params.active_folder.path + '</a></li>'));
                    }
                }*/
            };

            //////-------------------------------------------------------------

            this._prepareStatusbar = function () {

                $(megexplorer.ui.filtertoolbar).html("");


                var button = null;
                $.each(megexplorer.params.filter_presets, function (ind, el) {
                    button = $('<label class="btn btn-xs btn-default ' + (megexplorer.params.filter_preset === el.key ? 'active' : '') + '"> <input type="radio" name="filter-files" value="' + el.key + '" autocomplete="off" checked> ' + el.title + ' </label>');
                    $(megexplorer.ui.filtertoolbar).append(button);
                });

                $(megexplorer.ui.statusbar)
                    .html("")
                    .append('<span class="pull-left filter-label" style="margin-right:10px; font-size:12px; line-height:22px;">Filtrele:</span>')
                    .append(megexplorer.ui.filtertoolbar)
                    .append(megexplorer.ui.selectdiskbtn)
                    .append(megexplorer.ui.selectfolderbtn)
                    .append(megexplorer.ui.selectfilebtn);

            };

            //////-------------------------------------------------------------

            this._prepareToolbar = function () {
                $(megexplorer.ui.toolbar).html('');
                $(megexplorer.ui.toolbar).append(
                    $('<div class="btn-group"></div>')
                        .append(megexplorer.ui.goupbtn)
                );
                $(megexplorer.ui.toolbar).append(
                    $('<div class="btn-group"></div>')
                        .append(megexplorer.ui.createfolderbtn)
                        .append(megexplorer.ui.uploadbtn)
                );
                $(megexplorer.ui.toolbar).append(
                    $('<div class="btn-group pull-right"></div>')
                        .append(megexplorer.ui.deletebtn)
                        .append(megexplorer.ui.renamebtn)
                );
            };


            //////-------------------------------------------------------------

            this.steady = function () {
                // gizlenecek butonları gizle vs.

                $('input[name=active_disk_key]', megexplorer.ui.dropzone).val(megexplorer.params.active_disk ? megexplorer.params.active_disk.diskkey : null);
                $('input[name=active_folder_id]', megexplorer.ui.dropzone).val(megexplorer.params.active_folder ? megexplorer.params.active_folder.id : null);


                megexplorer._prepareBreadcrumb();
                megexplorer._prepareToolbar();
                megexplorer._prepareStatusbar();

                megexplorer.tidy.buttons();


                switch (megexplorer.params.mode) {
                    case "select_file":
                        $(megexplorer.ui.selectfilebtn).removeClass('hidden');
                        break;
                    case "select_folder":
                        $(megexplorer.ui.selectfolderbtn).removeClass('hidden');
                        break;
                    case "select_disk":
                        $(megexplorer.ui.selectdiskbtn).removeClass('hidden');
                        break;
                    default:
                        break;
                }


                //$(megexplorer.breadcrumb).html("").append('<li><a href="#">'+megexplorer.params.active_disk+'</a></li>').append('<li class="active"><a href="#">'+megexplorer.params.active_folder+'</a></li>');

            };


            this.tidy = {
                canGoUp: function () {
                    if (!megexplorer.params.active_disk) {
                        $(megexplorer.ui.goupbtn).prop('disabled', true);
                    } else {
                        $(megexplorer.ui.goupbtn).prop('disabled', false);
                    }
                    return this;
                },
                canUpload: function () {
                    if (!megexplorer.params.active_disk) {
                        $(megexplorer.ui.uploadbtn).prop('disabled', true);
                    } else {
                        $(megexplorer.ui.uploadbtn).prop('disabled', false);
                    }
                    return this;
                },
                canCreateFolder: function () {
                    if (!megexplorer.params.active_disk) {
                        $(megexplorer.ui.createfolderbtn).prop('disabled', true);
                    } else {
                        $(megexplorer.ui.createfolderbtn).prop('disabled', false);
                    }
                    return this;
                },
                closeDropzone: function () {
                    $('#dropzone', megexplorer.ui.wrapper).slideUp(100, function () {
                        $('.megexplorer-' + megexplorer.params.key + ' .megexplorer-upload-file').removeClass('on').addClass('off');
                        megexplorer.Dropzone.removeAllFiles(true);
                    });
                    return this;
                },
                buttons: function () {
                    this.canGoUp();
                    this.canUpload();
                    this.canCreateFolder();
                }

            };

            //////-------------------------------------------------------------

            this.diskSelect = function (disk_key) {

                if (window.opener)
                    window.opener.dispatchEvent(new CustomEvent('diskSelect', {detail: this.params.active_disk}));
                else
                    window.dispatchEvent(new CustomEvent('diskSelect', {detail: this.params.active_disk}));

                return alert("disk selected");

            };
            //////-------------------------------------------------------------

            this.folderSelect = function (folder_id) {

                if (window.opener)
                    window.opener.dispatchEvent(new CustomEvent('folderSelect', {detail: this.params.active_folder}));
                else
                    window.dispatchEvent(new CustomEvent('folderSelect', {detail: this.params.active_folder}));

                return alert("folder selected");

            };

            //////-------------------------------------------------------------

            this.fileSelect = function (file_id) {
                //this._selectItems(file_id);
                // selectFile(this.params.active_file);
                if (window.opener)
                    window.opener.dispatchEvent(new CustomEvent('fileSelect', {detail: this.params.active_file}));
                else
                    window.dispatchEvent(new CustomEvent('fileSelect', {detail: this.params.active_file}));

                return alert("file selected");
            };

            //////-------------------------------------------------------------

            this.xhrJson = function ($url, $params, $successCallback, $successParams, $failCallback, $failParams) {

                if ($params && $params["active_disk"] && $params["active_disk"]["root_files"]) {
                    delete $params["active_disk"]["root_files"];
                }
                if ($params && $params["active_folder"] && $params["active_folder"]["files"]) {
                    delete $params["active_folder"]["files"];
                }

                $defparams = {'_token': '{!! csrf_token() !!}', 'mode': megexplorer.params.mode};
                $results = $.ajax({
                    url: $url,
                    data: $params,
                    dataType: "json",
                    async: false/*,
                done:function(data) {window[$successCallback](data,$successParams);},
                fail:function(data) {window[$failCallback](data,$failParams);}*/
                }).responseJSON;


                return $results;
            };

            //////-------------------------------------------------------------


            this._listItems = function (items, params) {
                $(megexplorer.ui.listview).html("");
                if (typeof items.disks !== 'undefined') {
                    $.each(items.disks, function (ind, el) {
                        $disk = $(megexplorer.ui.diskmodel);
                        $($disk).data('diskmodel', el).attr('title', el.name);
                        $($disk).attr('data-disk_key', el.diskkey);
                        $('span.disk_name', $disk).html(el.name);
                        $(megexplorer.ui.listview).append($disk);
                    });
                }


                if (typeof items.folders !== 'undefined') {
                    $.each(items.folders, function (ind, el) {
                        if (el.path != "") {
                            $folder = $(megexplorer.ui.foldermodel);
                            $($folder).data('foldermodel', el).attr('title', el.path);
                            $($folder).attr('data-disk_key', megexplorer.params.active_disk.diskkey).attr('data-folder_id', el.id);
                            $('span.folder_name', $folder).html(el.path);
                            $(megexplorer.ui.listview).append($folder);
                        }
                    });
                }

                if (typeof items.files !== 'undefined') {
                    $.each(items.files, function (ind, el) {
                        $file = $(megexplorer.ui.filemodel);
                        $($file).data('filemodel', el).attr('title', el.name);
                        $($file).attr('data-disk_key', megexplorer.params.active_disk.diskkey).attr('data-folder_id', el.mfolder_id).attr('data-file_id', el.id);
                        var $preset = null;
                        for (var i = 0; i < megexplorer.params.filter_presets.length; i++) {
                            if (megexplorer.params.filter_presets[i].key == megexplorer.params.filter_preset) {
                                $preset = megexplorer.params.filter_presets[i];
                            }
                        }
                        var ext = megexplorer.getExtension(el.name);
                        $file.attr('data-ext', ext);
                        if (/^([a-zA-Z0-9._ıİşŞçÇüÜğĞöÖ\s-]*)?([.](jpg|jpeg|png|gif|bmp|))+$/.test(el.name)) {
                            var url = "{!! url(route('megexplorer.gotourlbase')) !!}/" + megexplorer.params.active_disk.diskkey + "/" + el.id + "?thumb=1";
                            $('div.img-holder > img', $file).attr("src", url);
                            $('i.fas, i.far', $file).remove();
                        } else {
                            $('div.img-holder', $file).remove();
                        }
                        if (!$preset.regex.test(el.name)) {
                            $($file).addClass('hidden');
                        }
                        $('span.file_name', $file).html(el.name);
                        $(megexplorer.ui.listview).append($file);
                    });
                }

            };

            //////-------------------------------------------------------------

            this.getSelectedFilterPreset = function () {

                for (var i = 0; i < megexplorer.params.filter_presets.length; i++) {
                    if (megexplorer.params.filter_presets[i].key === megexplorer.params.filter_preset) {
                        return megexplorer.params.filter_presets[i];
                    }
                }

                return null;

            };

            //////-------------------------------------------------------------


            this._clearSelection = function () {
                $(' .megexplorer-list-item', megexplorer.wrapper).removeClass('highlighted');
                $(megexplorer.ui.selectdiskbtn).prop('disabled', true).data('diskmodel', "");
                $(megexplorer.ui.selectfolderbtn).prop('disabled', true).data('foldermodel', "");
                $(megexplorer.ui.selectfilebtn).prop('disabled', true).data('filemodel', "");

                $(megexplorer.ui.deletebtn).attr('data-folder-id', "").attr('data-file-id', "").prop('disabled', true);
                $(megexplorer.ui.renamebtn).attr('data-folder-id', "").attr('data-file-id', "").prop('disabled', true);
            };

            //////-------------------------------------------------------------

            this._makeid = function () {
                let text = "";
                let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

                for (let i = 0; i < 5; i++) {
                    text += possible.charAt(Math.floor(Math.random() * possible.length));
                }

                return text;
            };

            //////-------------------------------------------------------------

            this.populateHandlers = function () {

                $('html').delegate('.megexplorer-' + this.params.key + ' .megexplorer-list-item.mdisk', 'dblclick', function (e) {
                    /*if (megexplorer.params.mode === "select_disk") {
                        return megexplorer.diskSelect($(this).attr('data-disk_key'));
                    }*/
                    m.navigate($(this).attr('data-disk_key'));

                    e.stopPropagation();
                });

                $('html').delegate('.megexplorer-' + this.params.key + ' .megexplorer-list-item.mfolder', 'dblclick', function (e) {
                    /*if (megexplorer.params.mode === "select_folder") {
                        return megexplorer.folderSelect($(this).attr('data-folder_id'));
                    }*/
                    megexplorer.navigate($(this).attr('data-disk_key'), $(this).attr('data-folder_id'));

                    e.stopPropagation();
                });

                $('html').delegate('.megexplorer-' + this.params.key + ' .megexplorer-list-item.mfile', 'dblclick', function (e) {
                    /*if (megexplorer.params.mode === "select_file") {
                        return megexplorer.fileSelect($(this).attr('data-file_id'));
                    }*/

                    e.stopPropagation();
                });

                $('html').delegate('.megexplorer-' + this.params.key + ' .megexplorer-list-item', 'click', function (e) {

                    megexplorer._clearSelection();
                    megexplorer.tidy.closeDropzone();


                    $(this).toggleClass('highlighted');

                    if ($(this).hasClass('highlighted')) {
                        if ($(this).hasClass('mdisk')) {
                            //console.log('mdisk');
                            //console.log($(this).data('diskmodel'));
                            $(megexplorer.ui.selectdiskbtn).data('diskmodel', $(this).data('diskmodel')).prop('disabled', false);
                        } else if ($(this).hasClass('mfolder')) {
                            //console.log('mfolder');
                            var fm = $(this).data('foldermodel');
                            console.log(fm);
                            $(megexplorer.ui.selectfolderbtn).data('foldermodel', $(this).data('foldermodel')).prop('disabled', false);
                            $(megexplorer.ui.deletebtn).attr("data-folder-id", ($(this).data('foldermodel') ? $(this).data('foldermodel').id : "")).data("file-id", null).prop('disabled', false);
                            $(megexplorer.ui.renamebtn).attr("data-folder-id", ($(this).data('foldermodel') ? $(this).data('foldermodel').id : "")).data("file-id", null).prop('disabled', false);
                        } else if ($(this).hasClass('mfile')) {
                            //console.log('mfile');
                            //console.log($(this).data('filemodel'));
                            //TODO
                            var fileId = parseInt(this.dataset.file_id);
                            if (megexplorer.params.active_folder) {
                                megexplorer.params.active_file = megexplorer.params.active_folder.files.find(function (el) {
                                    return el.id == fileId;
                                });
                            }
                            else {
                                megexplorer.params.active_file = megexplorer.params.active_disk.root_files.find(function (el) {
                                    return el.id == fileId;
                                });
                            }
                            $(megexplorer.ui.selectfilebtn).data('filemodel', $(this).data('filemodel')).prop('disabled', false);
                            $(megexplorer.ui.deletebtn).attr("data-folder-id", (megexplorer.params.active_folder ? megexplorer.params.active_folder.id : "")).attr("data-file-id", megexplorer.params.active_file.id).prop('disabled', false);
                            $(megexplorer.ui.renamebtn).attr("data-folder-id", (megexplorer.params.active_folder ? megexplorer.params.active_folder.id : "")).attr("data-file-id", megexplorer.params.active_file.id).prop('disabled', false);
                        }
                    }

                    e.stopPropagation();

                });

                $('html').delegate('.megexplorer-' + this.params.key + ' .megexplorer-select-file', 'click', function (e) {
                    callbacks[megexplorer.params.callback](megexplorer.params);
                    e.stopPropagation();
                });

                $('html').delegate('.megexplorer-' + this.params.key + ' .megexplorer-lv', 'click', function (e) {
                    megexplorer._clearSelection();
                    megexplorer.tidy.closeDropzone();
                });

                $('html').delegate('.megexplorer-' + this.params.key + ' .megexplorer-toolbar .btn', 'click', function (e) {
                    if ($(this).hasClass('megexplorer-delete') || $(this).hasClass('megexplorer-rename')) {
                        return false;
                    }
                    megexplorer._clearSelection();
                    if (!$(this).hasClass("megexplorer-upload-file")) {
                        megexplorer.tidy.closeDropzone();
                    }
                });

                $('html').delegate('.megexplorer-' + this.params.key + ' .megexplorer-toolbar', 'click', function (e) {
                    megexplorer._clearSelection();
                });

                $('html').delegate('.megexplorer-' + this.params.key + ' .megexplorer-create-folder', 'click', function (e) {
                    //e.preventDefault();
                    var folder_name = prompt('Dizin adı girin:');
                    if (!folder_name) {
                        alert("Cannot create folder without a name.");
                        return false;
                    }


                    megexplorer._createFolder(folder_name);


                });

                $('html').delegate('.megexplorer-' + this.params.key + ' .megexplorer-rename', 'click', function (e) {
                    var folder = $(this).attr('data-folder-id');
                    var file = $(this).attr('data-file-id');
                    var newName = prompt("Rename");
                    var rename = megexplorer._renameItem(newName, megexplorer.params.active_disk.diskkey, folder, file);
                    let swapdisabledstate = $(this).prop('disabled');
                    $('.fa', this).removeClass('fa-pen-alt').addClass('fa-spinner fa-spin').prop('disabled', true);

                    $('.fa', this).removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-pen-alt').prop('disabled', swapdisabledstate);

                    if (rename.status) {
                        megexplorer.steady();
                        megexplorer._clearSelection();
                        megexplorer.navigate(megexplorer.params.active_disk.diskkey, folder);

                    } else {
                        alert(rename.errors[0]);
                    }
                });

                $('html').delegate('.megexplorer-' + this.params.key + ' .megexplorer-delete', 'click', function (e) {
                    let swapdisabledstate = $(this).prop('disabled');
                    $('.fa', this).removeClass('fa-trash-alt').addClass('fa-spinner fa-spin').prop('disabled', true);

                    var remove = megexplorer._removeItem(megexplorer.params.active_disk.diskkey, $(this).data('folder-id'), $(this).data('file-id'));

                    $('.fa', this).removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-trash-alt').prop('disabled', swapdisabledstate);

                    if (remove.status) {
                        if (remove.req_state.file_id != -1) {
                            console.log('.megexplorer-' + megexplorer.params.key + ' .mfile[data-file_id=' + remove.req_state.file_id + ']');
                            $('.megexplorer-' + megexplorer.params.key + ' .mfile[data-file_id=' + remove.req_state.file_id + ']').remove();
                        } else if (remove.req_state.folder_id) {
                            console.log('.megexplorer-' + megexplorer.params.key + ' .mfolder[data-folder_id=' + remove.req_state.folder_id + ']');
                            $('.megexplorer-' + megexplorer.params.key + ' .mfolder[data-folder_id=' + remove.req_state.folder_id + ']').remove();
                        }

                        megexplorer.steady();
                        megexplorer._clearSelection();

                    } else {
                        alert(remove.errors[0]);
                    }


                });

                $('html').delegate('.megexplorer-' + this.params.key + ' .megexplorer-upload-file', 'click', function () {
                    if ($(this).hasClass('off')) {
                        $('#dropzone', megexplorer.ui.wrapper).slideDown(100, function () {
                            $('.megexplorer-' + megexplorer.params.key + ' .megexplorer-upload-file').removeClass('off').addClass('on');
                        });
                    } else {
                        $('#dropzone', megexplorer.ui.wrapper).slideUp(100, function () {
                            $('.megexplorer-' + megexplorer.params.key + ' .megexplorer-upload-file').removeClass('on').addClass('off');
                            megexplorer.Dropzone.removeAllFiles(true);
                        });
                    }
                });

                $('html').delegate('.megexplorer-' + this.params.key + ' .megexplorer-go-up', 'click', function (e) {

                    if ($(this).prop('disabled')) return false;
                    var disk = megexplorer.params.active_disk ? megexplorer.params.active_disk.diskkey : null;
                    var folder_in = megexplorer.params.active_folder ? megexplorer.params.active_folder.id : null;
                    var folder_up = megexplorer.params.active_folder ? megexplorer.params.active_folder.mfolder_id : null;
                    disk = folder_up ? disk : (folder_in ? disk : null);
                    megexplorer.navigate(disk, folder_up);
                });

                /*$('html').delegate('[data-context]', 'contextmenu', function (e) {
                    //e.preventDefault();
                    //var that = $(this);
                    //this.overlay.show();
                    //console.log(that.position());
                });*/


                $('html').delegate('.megexplorer-' + this.params.key + ' .megexplorer-status-bar input[type=radio][name=filter-files]', 'change click', function (e) {
                    megexplorer.params.filter_preset = this.value;
                    //alert(this.value);
                    megexplorer.applyFilter();
                });
            }
            //////-------------------------------------------------------------


        };


        ////////////////////////////////////////////
        var urlData = window.atob(deparam(window.location.search)["data"]);


        var m = new Megexplorer(JSON.parse(urlData == "" ? "{}" : urlData));

        $(document).ready(function () {
            m.init();
            //$("div#dropzone").dropzone({ url: "/file/post" });
        });


    </script>
@endpush