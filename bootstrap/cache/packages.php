<?php return array (
  'arcanedev/log-viewer' => 
  array (
    'providers' => 
    array (
      0 => 'Arcanedev\\LogViewer\\LogViewerServiceProvider',
    ),
  ),
  'cviebrock/laravel-elasticsearch' => 
  array (
    'providers' => 
    array (
      0 => 'Cviebrock\\LaravelElasticsearch\\ServiceProvider',
    ),
    'aliases' => 
    array (
      'Elasticsearch' => 'Cviebrock\\LaravelElasticsearch\\Facade',
    ),
  ),
  'fideloper/proxy' => 
  array (
    'providers' => 
    array (
      0 => 'Fideloper\\Proxy\\TrustedProxyServiceProvider',
    ),
  ),
  'intervention/image' => 
  array (
    'providers' => 
    array (
      0 => 'Intervention\\Image\\ImageServiceProvider',
    ),
    'aliases' => 
    array (
      'Image' => 'Intervention\\Image\\Facades\\Image',
    ),
  ),
  'kris/laravel-form-builder' => 
  array (
    'providers' => 
    array (
      0 => 'Kris\\LaravelFormBuilder\\FormBuilderServiceProvider',
    ),
    'aliases' => 
    array (
      'FormBuilder' => 'Kris\\LaravelFormBuilder\\Facades\\FormBuilder',
    ),
  ),
  'laravel/socialite' => 
  array (
    'providers' => 
    array (
      0 => 'Laravel\\Socialite\\SocialiteServiceProvider',
    ),
    'aliases' => 
    array (
      'Socialite' => 'Laravel\\Socialite\\Facades\\Socialite',
    ),
  ),
  'laravel/tinker' => 
  array (
    'providers' => 
    array (
      0 => 'Laravel\\Tinker\\TinkerServiceProvider',
    ),
  ),
  'laravelcollective/html' => 
  array (
    'providers' => 
    array (
      0 => 'Collective\\Html\\HtmlServiceProvider',
    ),
    'aliases' => 
    array (
      'Form' => 'Collective\\Html\\FormFacade',
      'Html' => 'Collective\\Html\\HtmlFacade',
    ),
  ),
  'maatwebsite/excel' => 
  array (
    'providers' => 
    array (
      0 => 'Maatwebsite\\Excel\\ExcelServiceProvider',
    ),
    'aliases' => 
    array (
      'Excel' => 'Maatwebsite\\Excel\\Facades\\Excel',
    ),
  ),
  'mediapress/bouncer' => 
  array (
    'providers' => 
    array (
      0 => 'Silber\\Bouncer\\BouncerServiceProvider',
    ),
    'aliases' => 
    array (
      'Bouncer' => 'Silber\\Bouncer\\BouncerFacade',
    ),
  ),
  'mediapress/mediapress' => 
  array (
    'providers' => 
    array (
      0 => 'Mediapress\\Providers\\MediapressServiceProvider',
    ),
  ),
  'mediapress/megexplorer' => 
  array (
    'providers' => 
    array (
      0 => 'Megexplorer\\Providers\\MegexplorerServiceProvider',
    ),
    'aliases' => 
    array (
      'Megexplorer' => 'Megexplorer\\Facades\\Megexplorer',
    ),
  ),
  'mediapress/mpassets' => 
  array (
    'providers' => 
    array (
      0 => 'Mpassets\\Providers\\MediapressAssetsServiceProvider',
    ),
  ),
  'nunomaduro/collision' => 
  array (
    'providers' => 
    array (
      0 => 'NunoMaduro\\Collision\\Adapters\\Laravel\\CollisionServiceProvider',
    ),
  ),
  'unisharp/laravel-filemanager' => 
  array (
    'providers' => 
    array (
      0 => 'UniSharp\\LaravelFilemanager\\LaravelFilemanagerServiceProvider',
    ),
    'aliases' => 
    array (
    ),
  ),
  'yajra/laravel-datatables-html' => 
  array (
    'providers' => 
    array (
      0 => 'Yajra\\DataTables\\HtmlServiceProvider',
    ),
  ),
  'yajra/laravel-datatables-oracle' => 
  array (
    'providers' => 
    array (
      0 => 'Yajra\\DataTables\\DataTablesServiceProvider',
    ),
    'aliases' => 
    array (
      'DataTables' => 'Yajra\\DataTables\\Facades\\DataTables',
    ),
  ),
);