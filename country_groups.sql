-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Anamakine: localhost
-- Üretim Zamanı: 30 Kas 2018, 14:33:39
-- Sunucu sürümü: 5.7.19-0ubuntu0.16.04.1
-- PHP Sürümü: 7.0.18-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `pilot_local`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `country_groups`
--

CREATE TABLE `country_groups` (
  `id` int(11) NOT NULL,
  `code` varchar(2) DEFAULT NULL,
  `title` varchar(190) CHARACTER SET utf8 NOT NULL,
  `owner` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Tablo döküm verisi `country_groups`
--

INSERT INTO `country_groups` (`id`, `code`, `title`, `owner`, `owner_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, NULL, 'Asya', 'Mediapress\\Modules\\Content\\Models\\Website', 1, '2018-11-20 14:11:08', '2018-11-20 14:11:08', NULL),
(2, NULL, 'Ortadoğu', 'Mediapress\\Modules\\Content\\Models\\Website', 1, '2018-11-20 14:12:41', '2018-11-29 15:51:09', '2018-11-29 15:51:09'),
(3, NULL, 'Güney Asya', 'Mediapress\\Modules\\Content\\Models\\Website', 1, '2018-11-20 14:31:39', '2018-11-20 14:31:39', NULL),
(4, NULL, 'Deneme', 'Mediapress\\Modules\\Content\\Models\\Website', 0, '2018-11-22 04:30:43', '2018-11-22 04:30:43', NULL),
(5, NULL, 'Deneme Grup', 'Mediapress\\Modules\\Content\\Models\\Website', 1, '2018-11-22 05:17:39', '2018-11-22 05:17:39', NULL),
(6, NULL, 'Yeni Grup', 'Mediapress\\Modules\\Content\\Models\\Website', 1, '2018-11-22 05:23:04', '2018-11-30 04:34:09', '2018-11-30 04:34:09'),
(7, NULL, 'Deneme 2', 'Mediapress\\Modules\\Content\\Models\\Website', 1, '2018-11-22 09:24:35', '2018-11-22 12:11:29', '2018-11-22 12:11:29'),
(8, NULL, 'Deneme 3', 'Mediapress\\Modules\\Content\\Models\\Website', 1, '2018-11-22 09:25:53', '2018-11-22 11:53:23', '2018-11-22 11:53:23'),
(9, NULL, 'Deneme 4', 'Mediapress\\Modules\\Content\\Models\\Website', 1, '2018-11-22 09:26:18', '2018-11-22 11:42:06', '2018-11-22 11:42:06'),
(10, NULL, 'Deneme 5', 'Mediapress\\Modules\\Content\\Models\\Website', 1, '2018-11-22 09:31:11', '2018-11-22 11:42:04', '2018-11-22 11:42:04'),
(11, NULL, 'Deneme 6', 'Mediapress\\Modules\\Content\\Models\\Website', 1, '2018-11-22 09:31:36', '2018-11-22 11:41:32', '2018-11-22 11:41:32'),
(12, NULL, 'Deneme 7', 'Mediapress\\Modules\\Content\\Models\\Website', 1, '2018-11-22 09:32:02', '2018-11-22 11:05:56', '2018-11-22 11:05:56'),
(13, NULL, 'Deneme 8', 'Mediapress\\Modules\\Content\\Models\\Website', 1, '2018-11-22 09:32:22', '2018-11-22 11:05:08', '2018-11-22 11:05:08'),
(14, NULL, 'Deneme 9', 'Mediapress\\Modules\\Content\\Models\\Website', 1, '2018-11-22 09:33:15', '2018-11-22 11:03:53', '2018-11-22 11:03:53'),
(15, NULL, 'Ws2 Group 1', 'Mediapress\\Modules\\Content\\Models\\Website', 2, '2018-11-22 13:41:44', '2018-11-22 13:41:44', NULL),
(16, NULL, 'deneme', 'Mediapress\\Modules\\Content\\Website', 0, '2018-11-23 05:49:22', '2018-11-23 05:49:22', NULL),
(17, NULL, 'Deneme', 'Mediapress\\Modules\\Content\\Models\\Website', 2, '2018-11-23 09:07:00', '2018-11-23 09:07:00', NULL),
(18, NULL, 'test', 'Mediapress\\Modules\\Content\\Models\\Website', 1, '2018-11-23 11:05:13', '2018-11-23 11:05:38', '2018-11-23 11:05:38'),
(19, NULL, 'deneme', 'Mediapress\\Modules\\Content\\Models\\Website', 127, '2018-11-23 11:20:57', '2018-11-23 11:20:57', NULL),
(20, NULL, 'deneme 2', 'Mediapress\\Modules\\Content\\Models\\Website', 127, '2018-11-23 11:21:08', '2018-11-23 11:21:08', NULL),
(21, NULL, 'Test', 'Mediapress\\Modules\\Content\\Models\\Website', 128, '2018-11-26 07:55:25', '2018-11-26 07:55:25', NULL),
(22, NULL, 'test', 'Mediapress\\Modules\\Content\\Models\\Website', 130, '2018-11-27 09:09:41', '2018-11-27 09:09:41', NULL),
(23, NULL, 'test', 'Mediapress\\Modules\\Content\\Models\\Website', 131, '2018-11-27 10:40:11', '2018-11-27 10:40:11', NULL),
(24, NULL, 'deneme', 'Mediapress\\Modules\\Content\\Models\\Website', 132, '2018-11-27 10:59:45', '2018-11-27 10:59:45', NULL),
(25, NULL, 'test', 'Mediapress\\Modules\\Content\\Models\\Website', 132, '2018-11-27 11:00:01', '2018-11-27 11:00:01', NULL),
(26, NULL, 'deneme', 'Mediapress\\Modules\\Content\\Models\\Website', 133, '2018-11-28 03:26:24', '2018-11-28 03:26:24', NULL),
(27, NULL, 'test', 'Mediapress\\Modules\\Content\\Models\\Website', 134, '2018-11-28 07:26:43', '2018-11-28 07:26:43', NULL),
(28, NULL, 'test2', 'Mediapress\\Modules\\Content\\Models\\Website', 133, '2018-11-28 09:09:39', '2018-11-28 09:09:39', NULL),
(29, NULL, 'deneme', 'Mediapress\\Modules\\Content\\Models\\Website', 136, '2018-11-29 05:38:57', '2018-11-29 05:38:57', NULL),
(30, NULL, 'deneme3', 'Mediapress\\Modules\\Content\\Models\\Website', 136, '2018-11-29 05:39:05', '2018-11-29 05:39:12', NULL),
(31, NULL, 'deneme2', 'Mediapress\\Modules\\Content\\Models\\Website', 136, '2018-11-29 05:40:04', '2018-11-29 05:40:04', NULL),
(32, NULL, 'test', 'Mediapress\\Modules\\Content\\Models\\Website', 137, '2018-11-29 07:48:24', '2018-11-29 07:48:24', NULL),
(33, NULL, 'test2', 'Mediapress\\Modules\\Content\\Models\\Website', 137, '2018-11-29 07:48:30', '2018-11-29 07:48:30', NULL),
(34, NULL, 'deneme', 'Mediapress\\Modules\\Content\\Models\\Website', 138, '2018-11-30 03:41:22', '2018-11-30 03:41:22', NULL),
(35, NULL, 'deneme2', 'Mediapress\\Modules\\Content\\Models\\Website', 138, '2018-11-30 03:41:29', '2018-11-30 03:41:29', NULL),
(36, NULL, 'Yeni Grup', 'Mediapress\\Modules\\Content\\Models\\Website', 1, '2018-11-30 04:33:42', '2018-11-30 04:35:27', NULL),
(37, NULL, 'test', 'Mediapress\\Modules\\Content\\Models\\Website', 140, '2018-11-30 08:08:06', '2018-11-30 08:08:06', NULL),
(38, NULL, 'test', 'Mediapress\\Modules\\Content\\Models\\Website', 141, '2018-11-30 08:26:06', '2018-11-30 08:26:06', NULL),
(39, NULL, 'test2', 'Mediapress\\Modules\\Content\\Models\\Website', 141, '2018-11-30 08:26:18', '2018-11-30 08:26:18', NULL);

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `country_groups`
--
ALTER TABLE `country_groups`
  ADD PRIMARY KEY (`id`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `country_groups`
--
ALTER TABLE `country_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
