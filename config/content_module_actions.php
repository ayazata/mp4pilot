<?php
return [
    "sitemap" => [
        'name' => 'Site Haritası',
        'name_affix' => "Ortak Yetkiler",
        'item_model' => \Mediapress\Modules\Content\Models\Sitemap::class,
        'item_id' => "*",
        'data' => [
            "is_root" => true,
            "is_variation" => false,
            "is_sub" => false,
            "descendant_of_sub" => false,
            "descendant_of_variation" => false
        ],
        'actions' => [
            'index' => [
                'name' => "Listele",
            ],
            'create' => [
                'name' => "Oluştur",
            ],
            'update' => [
                'name' => "Düzenle",
            ],
            'delete' => [
                'name' => "Sil",
            ],
        ],
        'subs' => [
            'detail' => [
                'name' => 'Sitemap Detayı',
                'item_model' => \Mediapress\Modules\Content\Models\SitemapDetail::class,
                'data' => [
                    "is_root" => false,
                    "is_variation" => false,
                    "is_sub" => true,
                    "descendant_of_sub" => false,
                    "descendant_of_variation" => false
                ],
                'actions' => [
                    /*'edit_apply' =>  [
                            'name' => "Güncelleme Öner",
                            'variables' =>['languages']
                    ],*/
                    'edit_update' => [
                        'name' => "Güncelle",
                        'variables' => ['languages']
                    ]
                ],
                'settings' => [
                    "auth_view_collapse" => "in"
                ]

            ],
            "page" => [
                'name' => 'Sayfa',
                'item_model' => \Mediapress\Modules\Content\Models\Page::class,
                'data' => [
                    "is_root" => false,
                    "is_variation" => false,
                    "is_sub" => true,
                    "descendant_of_sub" => false,
                    "descendant_of_variation" => false
                ],
                'actions' => [
                    'index' => [
                        'name' => "Listele",
                        'variables' => []
                    ],
                    'create' => [
                        'name' => "Oluştur",
                        'variables' => []
                    ],
                    'delete' => [
                        'name' => "Sil",
                    ]
                ],
                'subs' => [
                    'detail' => [
                        'name' => 'Sayfa Detayı',
                        'item_model' => \Mediapress\Modules\Content\Models\PageDetail::class,
                        'data' => [
                            "is_root" => false,
                            "is_variation" => false,
                            "is_sub" => true,
                            "descendant_of_sub" => true,
                            "descendant_of_variation" => false
                        ],
                        'actions' => [
                            /*'edit_apply' =>  [
                                    'name' => "Güncelleme Öner",
                                    'variables' =>['languages']
                            ],*/
                            'edit_update' => [
                                'name' => "Güncelle",
                                'variables' => ['languages']
                            ]
                        ],
                        'settings' => [
                            "auth_view_collapse" => "in"
                        ],
                    ],

                ],
                'settings' => [
                    "auth_view_collapse" => "in"
                ],
            ],
            "category" => [
                'name' => 'Kategori',
                'item_model' => \Mediapress\Modules\Content\Models\Category::class,
                'data' => [
                    "is_root" => false,
                    "is_variation" => false,
                    "is_sub" => true,
                    "descendant_of_sub" => false,
                    "descendant_of_variation" => false
                ],
                'actions' => [
                    'index' => [
                        'name' => "Listele",
                    ],
                    'create' => [
                        'name' => "Oluştur",
                    ],
                    'edit' => [
                        'name' => "Düzenle",
                    ],
                    'reorder' => [
                        'name' => "Sırala",
                    ],
                    'delete' => [
                        'name' => "Sil",
                    ]
                ],
                'subs' => [
                    'detail' => [
                        'name' => 'Kategori Detayı',
                        'item_model' => \Mediapress\Modules\Content\Models\CategoryDetail::class,
                        'data' => [
                            "is_root" => false,
                            "is_variation" => false,
                            "is_sub" => true,
                            "descendant_of_sub" => true,
                            "descendant_of_variation" => false
                        ],
                        'actions' => [
                            /*'edit_apply' =>  [
                                    'name' => "Güncelleme Öner",
                                    'variables' =>['languages']
                            ],*/
                            'edit_update' => [
                                'name' => "Güncelle",
                                'variables' => ['languages']
                            ]
                        ],
                    ],

                ],
            ],
            "criteria" => [
                'name' => 'Kriter',
                'item_model' => \Mediapress\Modules\Content\Models\Criteria::class,
                'data' => [
                    "is_root" => false,
                    "is_variation" => false,
                    "is_sub" => true,
                    "descendant_of_sub" => false,
                    "descendant_of_variation" => false
                ],
                'actions' => [
                    'index' => [
                        'name' => "Listele",
                    ],
                    'create' => [
                        'name' => "Oluştur",
                    ],
                    'edit' => [
                        'name' => "Düzenle",
                    ],
                    'reorder' => [
                        'name' => "Sırala",
                    ],
                    'delete' => [
                        'name' => "Sil",
                    ]
                ],
                'subs' => [
                    'detail' => [
                        'name' => 'Kriter Detayı',
                        'item_model' => \Mediapress\Modules\Content\Models\CriteriaDetail::class,
                        'data' => [
                            "is_root" => false,
                            "is_variation" => false,
                            "is_sub" => true,
                            "descendant_of_sub" => true,
                            "descendant_of_variation" => false
                        ],
                        'actions' => [
                            /*'edit_apply' =>  [
                                    'name' => "Güncelleme Öner",
                                    'variables' =>['languages']
                            ],*/
                            'edit_update' => [
                                'name' => "Güncelle",
                                'variables' => ['languages']
                            ]
                        ],
                    ],

                ],
            ],
            "property" => [
                'name' => 'Özellik',
                'item_model' => \Mediapress\Modules\Content\Models\Property::class,
                'data' => [
                    "is_root" => false,
                    "is_variation" => false,
                    "is_sub" => true,
                    "descendant_of_sub" => false,
                    "descendant_of_variation" => false
                ],
                'actions' => [
                    'index' => [
                        'name' => "Listele",
                    ],
                    'create' => [
                        'name' => "Oluştur",
                    ],
                    'edit' => [
                        'name' => "Düzenle",
                    ],
                    'reorder' => [
                        'name' => "Sırala",
                    ],
                    'delete' => [
                        'name' => "Sil",
                    ]
                ],
                'subs' => [
                    'detail' => [
                        'name' => 'Özellik Detayı',
                        'item_model' => \Mediapress\Modules\Content\Models\PropertyDetail::class,
                        'data' => [
                            "is_root" => false,
                            "is_variation" => false,
                            "is_sub" => true,
                            "descendant_of_sub" => true,
                            "descendant_of_variation" => false
                        ],
                        'actions' => [
                            /* 'edit_apply' =>  [
                                     'name' => "Güncelleme Öner",
                                     'variables' =>['languages']
                             ],*/
                            'edit_update' => [
                                'name' => "Güncelle",
                                'variables' => ['languages']
                            ]
                        ],
                    ],

                ],
            ],

        ],
        'variations' => function ($root_item) {
            $sitemaps = \Mediapress\Modules\Content\Models\Sitemap::with('detail')->get();
            $sets = [];

            foreach ($sitemaps as $sm) {
                if ( ! $sm->detail) {
                    continue;
                }
                $set = [];
                $set["name"] = 'Site H. ' . $sm->detail->name;
                $set['item_model'] = \Mediapress\Modules\Content\Models\Sitemap::class;
                $set['item_id'] = $sm->id;
                $set['actions'] = [
                    'update' => ['name' => "Düzenle",],
                    'delete' => ['name' => "Sil",]
                ];
                $set['settings'] = [
                    "auth_view_collapse" => "out"
                ];
                $set['data'] = [
                    "is_root" => false,
                    "is_variation" => true,
                    "is_sub" => false,
                    "descendant_of_sub" => false,
                    "descendant_of_variation" => false
                ];
                $set['subs'] = $root_item['subs'];
                data_set($set['subs'], '*.data.descendant_of_variation', true);
                $sets['sitemap' . $sm->id] = $set;
            }

            return $sets;
        },
        'settings' => [
            "auth_view_collapse" => "in"
        ]

    ],
];