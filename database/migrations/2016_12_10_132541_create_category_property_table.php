<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoryPropertyTable extends Migration
{

    public function up()
    {
        if (!Schema::hasTable('category_property')) {
            Schema::create('category_property', function (Blueprint $table) {
                $table->integer('category_id')->unsigned();
                $table->integer('property_id')->unsigned();
            });
        }
    }

    public function down()
    {
        Schema::drop('category_property');
    }
}