<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUrlsTable extends Migration
{

    public function up()
    {
        Schema::create('websites', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("order");
            $table->string("name");
            $table->string("domain");
            $table->boolean("has_ssl")->default(false);
            $table->integer("website_id")->nullable();
            $table->integer("use_deflng_code")->default(false);
            $table->timestamps();
            $table->softDeletes();

            $table->unique("domain", "domain_unique");
        });
    }

    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropUnique("domain_unique");
        Schema::dropIfExists('website');
        Schema::enableForeignKeyConstraints();
    }
}