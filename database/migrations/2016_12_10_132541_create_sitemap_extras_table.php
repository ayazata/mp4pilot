<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSitemapExtrasTable extends Migration {

	public function up()
	{
		Schema::create('sitemap_extras', function(Blueprint $table) {
		    $table->increments('id');
			$table->integer('sitemap_id')->index();
			$table->string('key');
			$table->text('value');
		});
	}

	public function down()
	{
		Schema::drop('extras');
	}
}