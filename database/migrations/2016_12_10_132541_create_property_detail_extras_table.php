<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePropertyDetailExtrasTable extends Migration
{

    public function up()
    {
        if (!Schema::hasTable('property_detail_extras')) {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
            $schema->create('property_detail_extras', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('property_detail_id')->unsigned()->nullable()->index();
                $table->string('key');
                $table->text('value');
            });
        }
    }

    public function down()
    {
        Schema::drop('property_detail_extras');
    }
}
