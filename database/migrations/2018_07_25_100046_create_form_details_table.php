<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (!Schema::hasTable('form_details')) {
            Schema::create('form_details', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('form_id');
                $table->string('label')->nullable();
                $table->string('name')->nullable();
                $table->string('type')->nullable();
                $table->string('rules')->nullable();
                $table->text('parameters')->nullable();
                $table->string('model')->nullable();
                $table->string('where')->nullable();
                $table->integer('order')->default(1);
                $table->string('values')->nullable();
                $table->integer('list_option')->nullable();
                $table->string('select')->nullable();
                $table->string('div_attr')->nullable();
                $table->string('region_status')->nullable();
                $table->string('region_label')->nullable();
                $table->string('region_name')->nullable();
                $table->string('text')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('form_details');
        Schema::enableForeignKeyConstraints();
    }
}
