<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            
            $table->increments('id')->unsigned();
            $table->integer("sitemap_id")->unsigned();
            $table->integer("page_id")->unsigned();
            $table->integer("admin_id")->unsigned();
            $table->integer("order")->unsigned();
            $table->integer("lft")->unsigned();
            $table->integer("rgt")->unsigned();
            $table->date("date")->unsigned();
            $table->tinyInteger("status")->unsigned()   ;
            $table->timestamps("published_at");

            $table->text("ctex_1")->nullable();
            $table->text("ctex_2")->nullable();

            $table->string("cvar_1")->nullable();
            $table->string("cvar_2")->nullable();
            $table->string("cvar_3")->nullable();
            $table->string("cvar_4")->nullable();

            $table->integer("cint_1")->nullable();
            $table->string("cint_2")->nullable();
            $table->string("cint_3")->nullable();
            $table->string("cint_4")->nullable();

            $table->date("cdat_1")->nullable();
            $table->date("cdat_2")->nullable();

            $table->decimal("cdec_1", 20,4)->nullable();
            $table->decimal("cdec_2", 20,4)->nullable();
            $table->decimal("cdec_3", 20,4)->nullable();
            $table->decimal("cdec_4", 20,4)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
