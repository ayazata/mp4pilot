<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSitemapTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sitemap_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string("name");
            $table->string("controller");
            $table->string("external_controller")->nullable();
            $table->unsignedTinyInteger("external_required")->default(0);
            $table->string("sitemap_type_id");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sitemap_types');
    }
}
