<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUrlsTable extends Migration
{

    public function up()
    {
        Schema::create('urls', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('website_id');
            $table->string('url');
            $table->morphs("model");
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('website_id')->references('id')->on('websites')->onDelete('cascade');
            $table->unique(["website_id", "url"], "url_unique");
        });

        Schema::create('dynamic_urls', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('url_id');
            $table->string('regex');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('url_id')->references('id')->on('urls')->onDelete('cascade');
            $table->unique(["website_id", "url_id"], "dynamic_url_unique");
        });

        Schema::create('redirect_urls', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('website_id');
            $table->string('url');
            $table->morphs("model");
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('website_id')->references('id')->on('websites')->onDelete('cascade');
            $table->unique(["website_id", "url"], "redirect_url_unique");
        });
    }

    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropUnique("url_unique");
        Schema::dropUnique("dynamic_url_unique");
        Schema::dropUnique("redirect_url_unique");
        Schema::dropIfExists('urls');
        Schema::dropIfExists('dynamic_urls');
        Schema::dropIfExists('redirect_urls');
        Schema::enableForeignKeyConstraints();
    }
}