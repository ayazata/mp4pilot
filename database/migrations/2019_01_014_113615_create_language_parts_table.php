<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLanguagePartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('language_parts'))
            return;

        Schema::create('language_parts', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('language_id')->unsigned()->nullable();
            $table->integer('country_group_id')->unsigned()->nullable();
            $table->integer('website_id')->unsigned()->nullable();
            $table->string('key');
            $table->text('value')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('language_parts');
    }
}
