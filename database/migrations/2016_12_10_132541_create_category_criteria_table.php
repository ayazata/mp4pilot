<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoryCriteriaTable extends Migration
{

    public function up()
    {
        if (!Schema::hasTable('category_criteria')) {
            Schema::create('category_criteria', function (Blueprint $table) {
                $table->integer('category_id')->unsigned();
                $table->integer('criteria_id')->unsigned();
            });
        }
    }

    public function down()
    {
        Schema::drop('category_criteria');
    }
}