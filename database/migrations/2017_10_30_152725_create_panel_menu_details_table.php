<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePanelMenuDetailsTable extends Migration {

	public function up()
	{
		Schema::create('panel_menu_details', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('menu_id')->unsigned();
			$table->integer('sitemap_id')->unsigned();
            $table->integer('parent')->nullable()->unsigned();
            $table->integer("lft")->unsigned()->nullable();
            $table->integer("rgt")->unsigned()->nullable();
            $table->integer("depth")->unsigned()->nullable();
            $table->tinyInteger("draft")->default(1);
            $table->timestamps();
            $table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('panel_menu_details');
	}
}