<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePanelMenuTable extends Migration {

	public function up()
	{
		Schema::create('panel_menu', function(Blueprint $table) {
			$table->increments('id');
            $table->integer("website_id")->nullable();
			$table->string('name')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('panel_menu');
	}
}