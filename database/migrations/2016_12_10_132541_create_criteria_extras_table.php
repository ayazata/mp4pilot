<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCriteriaExtrasTable extends Migration
{

    public function up()
    {
        if (!Schema::hasTable('criteria_extras')) {
            Schema::create('criteria_extras', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('criteria_id')->unsigned()->nullable()->index();
                $table->string('key');
                $table->text('value');
            });
        }
    }

    public function down()
    {
        Schema::drop('criteria_extras');
    }
}