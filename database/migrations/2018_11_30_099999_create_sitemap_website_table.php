<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sitemap_details', function (Blueprint $table) {

            $table->increments('id')->unsigned();
            $table->integer("sitemap_id")->unsigned();
            $table->integer("language_id")->unsigned();
            $table->string("country_group_id", 2)->nullable();
            $table->integer("website_id")->unsigned();
            $table->string("name");
            $table->string("slug");
            $table->string("category_slug");
            $table->boolean("active");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sitemap_details');
    }
}
