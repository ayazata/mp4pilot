<?php

use Illuminate\Database\Migrations\Migration;
use Mediapress\Modules\MPCore\Foundation\Blueprint;

class CreatePropertiesTable extends Migration
{

    public function up()
    {
        if (!Schema::hasTable('properties')) {
            Schema::create('properties', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('sitemap_id')->unsigned()->index();
                $table->boolean('type')->nullable();

                $table->defaultFields();
                $table->nestable("property_id");

                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    public function down()
    {
        Schema::drop('properties');
    }
}