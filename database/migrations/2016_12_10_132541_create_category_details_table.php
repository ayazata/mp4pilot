<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoryDetailsTable extends Migration {

	public function up()
	{
		Schema::create('category_details', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('category_id')->unsigned()->index();
			$table->integer('language_id')->unsigned()->index();
			$table->string('name');
			$table->string('slug')->nullable();
			$table->longText('detail')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('category_details');
	}
}