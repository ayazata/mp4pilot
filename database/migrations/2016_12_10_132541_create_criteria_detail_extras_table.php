<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCriteriaDetailExtrasTable extends Migration
{

    public function up()
    {
        if (!Schema::hasTable('criteria_detail_extras')) {
            Schema::create('criteria_detail_extras', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('criteria_detail_id')->unsigned()->nullable()->index();
                $table->string('key');
                $table->text('value');
            });
        }
    }

    public function down()
    {
        Schema::drop('criteria_detail_extras');
    }
}