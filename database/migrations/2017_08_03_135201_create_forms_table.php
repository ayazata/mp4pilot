<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        if (!Schema::hasTable('forms')) {
            Schema::create('forms', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id');
                $table->string('name')->nullable();
                $table->string('slug')->nullable();
                $table->string('sender')->nullable();
                $table->string('password')->nullable();
                $table->string('formbuilder_json')->nullable();
                $table->string('smtp')->nullable();
                $table->string('port')->nullable();
                $table->string('security')->nullable();
                $table->string('receiver')->nullable();
                $table->string('success')->nullable();
                $table->string('error')->nullable();
                $table->string('button')->nullable();
                $table->string('captcha')->nullable();
                $table->string('captcha_site_key')->nullable();
                $table->string('captcha_secret')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('forms');
        Schema::enableForeignKeyConstraints();
    }
}
