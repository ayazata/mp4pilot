<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCriteriaDetailsTable extends Migration
{

    public function up()
    {
        if (!Schema::hasTable('criteria_details')) {
            Schema::create('criteria_details', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('criteria_id')->unsigned()->index();
                $table->integer('country_group_id')->unsigned()->index();
                $table->integer('language_id')->unsigned()->index();
                $table->string('name');
                $table->string('slug')->nullable();
                $table->text('detail');
            });
        }
    }

    public function down()
    {
        Schema::drop('criteria_details');
    }
}