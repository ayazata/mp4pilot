<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoryPageTable extends Migration
{

    public function up()
    {
        if (!Schema::hasTable('category_page')) {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
            $schema->create('category_page', function (Blueprint $table) {
                $table->integer('category_id')->unsigned();
                $table->integer('page_id')->unsigned();
            });
        }
    }

    public function down()
    {
        Schema::drop('category_page');
    }
}
