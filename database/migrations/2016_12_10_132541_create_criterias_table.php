<?php

use Illuminate\Database\Migrations\Migration;
use Mediapress\Modules\MPCore\Foundation\Blueprint;

class CreateCriteriasTable extends Migration
{

    public function up()
    {
        if (!Schema::hasTable('criterias')) {
            Schema::create('criterias', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('sitemap_id')->unsigned()->index();
                $table->boolean('type')->nullable();

                $table->defaultFields();
                $table->nestable("criteria_id");

                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    public function down()
    {
        Schema::drop('criterias');
    }
}