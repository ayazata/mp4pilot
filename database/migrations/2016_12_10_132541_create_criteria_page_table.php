<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCriteriaPageTable extends Migration
{

    public function up()
    {
        if (!Schema::hasTable('criteria_page')) {
            Schema::create('criteria_page', function (Blueprint $table) {
                $table->integer('criteria_id')->unsigned()->index();
                $table->integer('page_id')->unsigned()->index();
            });
        }
    }

    public function down()
    {
        Schema::drop('criteria_page');
    }
}