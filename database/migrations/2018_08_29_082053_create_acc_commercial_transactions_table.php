<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAccCommercialTransactionsTable extends Migration {

	public function up()
	{
		Schema::create('acc_commercial_transactions', function(Blueprint $table) {
			$table->increments('id');
            $table->integer('website_id')->nullable();
            $table->enum('receivable_payable', array('receivable', 'payable'));
            $table->string('name', 250);
            $table->integer('transaction_type_id');
            $table->enum('system_entity', array('system', 'entity'));
            $table->integer('entity_userlist_pivot_id');
			$table->float('amount', 10,4);
			$table->integer('currency_type');
			$table->string('note')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('acc_commercial_transactions');
	}
}