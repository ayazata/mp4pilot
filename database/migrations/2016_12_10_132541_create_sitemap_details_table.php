<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSitemapDetailsTable extends Migration
{

    public function up()
    {
        if (!Schema::hasTable('sitemap_details')) {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
            $schema->create('sitemap_details', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('sitemap_id')->unsigned()->index();
                $table->integer('language_id')->unsigned()->index();
                $table->unsignedInteger('country_group_id')->nullable()->index();
                $table->unsignedInteger('website_id');
                $table->string('name');
                $table->string('slug')->nullable();
                $table->string('category_slug')->nullable();
                $table->boolean('active');
            });
        }
    }

    public function down()
    {
        Schema::drop('sitemap_details');
    }
}
