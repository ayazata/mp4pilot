<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePropertyPageTable extends Migration
{

    public function up()
    {
        if (!Schema::hasTable('property_page')) {
            Schema::create('property_page', function (Blueprint $table) {
                $table->integer('property_id')->unsigned()->index();
                $table->integer('page_id')->unsigned()->index();
            });
        }
    }

    public function down()
    {
        Schema::drop('property_page');
    }
}