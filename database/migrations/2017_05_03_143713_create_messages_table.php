<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        if (!Schema::hasTable('message')) {
            Schema::create('message', function (Blueprint $table) {
                $table->increments('id');
                $table->string('subject')->nullable();
                $table->string('mail')->nullable();
                $table->string('ip')->nullable();
                $table->text('data');
                $table->integer('read')->default('0');
                $table->integer('file')->default('0');
                $table->integer('deleted')->default('0');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('message');
        Schema::enableForeignKeyConstraints();
    }
}
