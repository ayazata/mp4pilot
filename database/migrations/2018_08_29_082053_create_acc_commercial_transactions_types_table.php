<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAccCommercialTransactionsTypesTable extends Migration {

	public function up()
	{
		Schema::create('acc_commercial_transactions_types', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name');
		});
	}

	public function down()
	{
		Schema::drop('acc_commercial_transactions_types');
	}
}