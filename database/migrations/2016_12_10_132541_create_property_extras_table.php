<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePropertyExtrasTable extends Migration
{

    public function up()
    {
        if (!Schema::hasTable('property_extras')) {
            Schema::create('property_extras', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('property_id')->unsigned()->nullable()->index();
                $table->string('key');
                $table->text('value');
            });
        }
    }

    public function down()
    {
        Schema::drop('property_extras');
    }
}