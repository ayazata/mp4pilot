<?php

use Illuminate\Database\Migrations\Migration;
use Mediapress\Modules\MPCore\Foundation\Blueprint;

class CreateLanguageWebsiteTable extends Migration
{

    public function up()
    {
        Schema::create('language_website', function (Blueprint $table) {
            $table->integer('language_id');
            $table->integer('website_id');
            $table->integer('country_group_id')->nullable();
            $table->boolean('default');
        });
    }

    public function down()
    {
        Schema::dropIfExists('language_website');
    }
}
