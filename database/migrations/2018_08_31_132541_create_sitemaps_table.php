<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSitemapsTable extends Migration
{

    public function up()
    {
        Schema::create('sitemaps', function (Blueprint $table) {
            $table->increments('id');

            $table->string("type")->default("");

            $table->boolean("has_detail")->default(false);
            $table->boolean("has_category")->default(false);
            $table->boolean("has_criteria")->default(false);
            $table->boolean("has_property")->default(false);
            $table->boolean("searchable")->default(false);

            $table->integer("website_id")->nullable();

            $table->timestamps();
            $table->softDeletes();

        });

        Schema::create("sitemap_website", function (Blueprint $table) {
            $table->integer("website_id", false, true);
            $table->integer("sitemap_id", false, true);
            $table->index(["website_id", "sitemap_id"], "website_sitemap_index");
        });
    }

    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIndex("website_sitemap_index");
        Schema::dropIfExists('sitemaps');
        Schema::dropIfExists('sitemap_website');
        Schema::enableForeignKeyConstraints();
    }
}