<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePropertyDetailsTable extends Migration
{

    public function up()
    {
        if (!Schema::hasTable('property_details')) {
            Schema::create('property_details', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('property_id')->unsigned()->index();
                $table->integer('country_group_id')->unsigned()->index();
                $table->integer('language_id')->unsigned()->index();
                $table->string('name');
                $table->string('slug')->nullable();
                $table->text('detail');
            });
        }
    }

    public function down()
    {
        Schema::drop('property_details');
    }
}