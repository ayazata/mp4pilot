<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoriesTable extends Migration {

	public function up()
	{
		Schema::create('categories', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('sitemap_id')->unsigned()->index();
			$table->integer('category_id')->unsigned()->nullable()->index();
			$table->string('category_tag',100)->nullable();
            $table->integer("lft")->unsigned()->nullable();
            $table->integer("rgt")->unsigned()->nullable();
            $table->integer("depth")->unsigned()->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('categories');
	}
}