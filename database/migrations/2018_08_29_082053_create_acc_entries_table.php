<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAccEntriesTable extends Migration {

	public function up()
	{
		Schema::create('acc_entries', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('commercial_transcation_id')->unsigned();
			$table->integer('payment_type')->nullable();
			$table->decimal('amount', 10,4);
            $table->string('note')->nullable();
            $table->date('payment_date')->nullable();
            $table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('acc_entries');
	}
}