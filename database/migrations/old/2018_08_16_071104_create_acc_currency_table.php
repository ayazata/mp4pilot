<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAccCurrencyTable extends Migration {

	public function up()
	{
		Schema::create('acc_currency', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('website_id')->nullable();
			$table->string('name', 100);
			$table->string('code', 100);
		});
	}

	public function down()
	{
		Schema::drop('acc_currency');
	}
}