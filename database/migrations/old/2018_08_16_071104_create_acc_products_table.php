<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAccProductsTable extends Migration {

    public function up()
    {
        Schema::create('acc_products', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('website_id')->nullable();
            $table->string('name', 255);
            $table->decimal('cost_price', 10,2);
            $table->decimal('sale_price', 10,2);
            $table->integer('tax_rate');
            $table->timestamps("created_at")->useCurrent();
            $table->timestamps("updated_at")->useCurrent();
        });
    }

    public function down()
    {
        Schema::drop('acc_products');
    }
}