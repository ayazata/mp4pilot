<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAccEntriesTable extends Migration {

	public function up()
	{
		Schema::create('acc_entries', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('website_id')->nullable();
			$table->integer('commercial_transaction_id');
			$table->integer('userlist_id')->nullable();
			$table->integer('userlist_pivot_id');
			$table->string('product_id');
			$table->string('payment_type_id');
			$table->enum('in_out', array('in', 'out'));
			$table->integer('wallet_type');
			$table->decimal('amount', 10,2);
			$table->integer('currency_id');
			$table->string('note')->nullable();
			$table->integer('sub_entry_id')->nullable();
            $table->timestamps("created_at")->useCurrent();
            $table->timestamps("updated_at")->useCurrent();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('acc_entries');
	}
}