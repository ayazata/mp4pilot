<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAccPaymentTypesTable extends Migration {

	public function up()
	{
		Schema::create('acc_payment_types', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('website_id')->nullable();
			$table->string('name');
		});
	}

	public function down()
	{
		Schema::drop('acc_payment_types');
	}
}