<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAccSubEntriesTable extends Migration {

	public function up()
	{
		Schema::create('acc_sub_entries', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('entry_id');
			$table->string('name')->nullable();
			$table->decimal('amount', 10,2);
			$table->integer('currency_id');
			$table->timestamps("created_at")->useCurrent();
			$table->timestamps("updated_at")->useCurrent();
		});
	}

	public function down()
	{
		Schema::drop('acc_sub_entries');
	}
}