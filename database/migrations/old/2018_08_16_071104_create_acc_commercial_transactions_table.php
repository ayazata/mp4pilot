<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAccCommercialTransactionsTable extends Migration {

	public function up()
	{
		Schema::create('acc_commercial_transactions', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('website_id')->nullable();
			$table->string('name', 250);
		});
	}

	public function down()
	{
		Schema::drop('acc_commercial_transactions');
	}
}