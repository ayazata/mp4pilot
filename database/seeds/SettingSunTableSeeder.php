<?php

use Illuminate\Database\Seeder;

class SettingSunTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //delete users table records
        DB::table('setting_sun')->delete();
        //insert some dummy records
        DB::table('setting_sun')->insert([
            ['website_id'=>null, 'group'=>'E-Posta Yönetimi', 'title'=>'Mailchimp API Key','key'=>'heraldist.mailchimp.apikey', 'value'=>'a7376c4ec8bf6dd27d24266bd01808df-us18', 'vtype'=>'string', 'params'=>null],
            ['website_id'=>null, 'group'=>'E-Posta Yönetimi', 'title'=>'Gönderici E-Posta','key'=>'heraldist.mpmailler.fromemailaddress', 'value'=>'default@default.com', 'vtype'=>'string', 'params'=>null],
            ['website_id'=>null, 'group'=>'Varsayılan Gönderici Adı', 'title'=>'Gönderici E-Posta','key'=>'heraldist.mpmailler.fromname', 'value'=>'test', 'vtype'=>'string', 'params'=>null],
            ['website_id'=>null, 'group'=>'Analytics', 'title'=>'Yandex Metrica Cache','key'=>'mpcore.analytics.yandex.cache', 'value'=>'600', 'vtype'=>'string', 'params'=>null],
            ['website_id'=>null, 'group'=>'Analytics', 'title'=>'Yandex Metrica Counter ID (Sayaç ID)','key'=>'mpcore.analytics.yandex.counter_id', 'value'=>'49873495', 'vtype'=>'string', 'params'=>null],
            ['website_id'=>null, 'group'=>'Analytics', 'title'=>'Yandex Metrica Token','key'=>'mpcore.analytics.yandex.token', 'value'=>'AQAAAAAo4lphAAUjimZh6CfDoEkevy2m8jNLwt0', 'vtype'=>'string', 'params'=>null],
            ['website_id'=>null, 'group'=>'Analytics', 'title'=>'Bing API Key','key'=>'mpcore.analytics.bing.apikey', 'value'=>'5d8fb140135c40f28a09042f7fe81bff', 'vtype'=>'string', 'params'=>null],
            ['website_id'=>null, 'group'=>'Analytics', 'title'=>'Bing API website URL','key'=>'mpcore.analytics.bing.api_website_url', 'value'=>'http://test.com', 'vtype'=>'string', 'params'=>null],
        ]);
    }
}
