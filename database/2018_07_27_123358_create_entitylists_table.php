<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserlistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entitylist', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('list_type', ['standard','core'])->nullable();
            $table->string('name');
            $table->string('description')->nullable();
            $table->integer('status');
            $table->timestamps();
        });

        // Pivot With websites
        Schema::create('entitylist_website', function (Blueprint $table) {
            $table->integer('entitylist_id');
            $table->integer('website_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('userlists');
    }
}
