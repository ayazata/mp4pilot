<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        if (!Schema::hasTable('users')) {
            Schema::table('users', function (Blueprint $table) {
                $table->string('phone')->nullable()->after("remember_token");
                $table->string('provider')->nullable()->after("phone");
                $table->string('provider_id')->nullable()->after("provider");
                $table->enum('gender', ['male', 'female'])->after("phone");
                $table->date('birthday')->nullable()->after("gender");
                $table->boolean('phone_valid')->default(false)->after("birthday");
                $table->boolean('activated')->default(false)->after("phone_valid");
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('users','phone')){
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('phone');
                $table->dropColumn('provider');
                $table->dropColumn('provider_id');
                $table->dropColumn('gender');
                $table->dropColumn('birthday');
                $table->dropColumn('phone_valid');
                $table->dropColumn('activated');
            });
        }

    }
}
