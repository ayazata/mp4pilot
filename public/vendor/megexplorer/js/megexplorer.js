
function xhrJson($url, $params, success, error){
    $.ajax({
        url: $url,
        data: {_token: ''},
        dataType: "json"
    }).success(function (data) {
        success(data);
    }).error(function (data) {
        error(data);
    });
}