//custom event polyfill
(function () {

    if (typeof window.CustomEvent === "function") return false;

    function CustomEvent(event, params) {
        params = params || {bubbles: false, cancelable: false, detail: undefined};
        var evt = document.createEvent('CustomEvent');
        evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
        return evt;
    }

    CustomEvent.prototype = window.Event.prototype;

    window.CustomEvent = CustomEvent;
})();


const DetailsEngine = function ($params) {

    const engine = this;

    let default_params = {
        capsules_container: $("html"),
        slug_lookup_url: "/mp-admin/Content/checkUrl",
        meta_variables_url: "/mp-admin/Content/getMetaVariablesFor"
    };


    this.params = $.extend(default_params, $params);

    this.init = function () {
        $(".detail-capsule", $(engine.params.capsules_container)).each(function (ind, el) {
            let slugworker = new SlugWorker({
                "capsule": $(el),
                "url": engine.params.slug_lookup_url,
                "details_engine": engine
            });
            $(el).data("detail-slug-worker", slugworker);

            let metaworker = new MetaWorker({
                "capsule": $(el),
                "url": engine.params.meta_variables_url,
                "details_engine": engine
            });
            $(el).data("detail-meta-worker", metaworker);

            $(".detail-name", el).on("change", function (e) {
                const event = document.createEvent('CustomEvent');
                event.initCustomEvent('detailNameChange', true, true, {DOMElement: this, originalEvent: e});
                document.dispatchEvent(event);
            });

            $(".detail-detail", el).on("change", function (e) {
                const event = document.createEvent('CustomEvent');
                event.initCustomEvent('detailDetailChange', true, true, {DOMElement: this, originalEvent: e});
                document.dispatchEvent(event);
            });

            $(".slug-control", el).on("change", function (e) {
                const event = document.createEvent('CustomEvent');
                event.initCustomEvent('detailSlugChange', true, true, {DOMElement: this, originalEvent: e});
                document.dispatchEvent(event);
            });

        });
        return engine;
    };
};


const SlugWorker = function ($params) {


    const worker = this;

    let default_params = {
        capsule: null,
        url: "",
        details_engine: null
    };

    this.params = $.extend(default_params, $params);

    this.capsule = null;
    this.setCapsule = function (capsule) {
        worker.capsule = capsule;
    };
    this.getCapsule = function () {
        return $(worker.capsule);
    };

    this.url = null;
    this.setUrl = function (url) {
        worker.url = url;
    };
    this.getUrl = function () {
        return worker.url;
    };

    this.engine = null;
    this.setEngine = function (engine) {
        worker.engine = engine;
    };
    this.getEngine = function () {
        return worker.engine;
    };


    this.control = null;

    this.init = function () {
        worker.setCapsule($(worker.params.capsule));
        worker.control = $(".detail-slug-control", worker.capsule);
        worker.setUrl(worker.params.url);
        worker.setEngine(worker.params.details_engine);
        worker.tieHandlers();
        return worker;
    };

    this.pickDetailDataFromCapsule = function () {
        return {
            "detail_type": $(worker.capsule).data("detail-type"),
            "detail_id": $(worker.capsule).data("detail-id"),
            "parent_id": $(worker.capsule).data("parent-id"),
            "language_id": $(worker.capsule).data("language-id"),
            "country_group_id": $(worker.capsule).data("country-group-id")
        };
    };

    this.getValidSlug = function (sluggable_text) {
        let detail_data = worker.pickDetailDataFromCapsule();
        let token = $("input[name='_token']").val();
        let newslug = $.ajax({
            url: worker.url,//http://pilot.local/mp-admin/Content/Pages/checkUrl
            method: 'POST',
            async: false,
            dataType: "json",
            data: {
                "_token": token,
                "detail_type": detail_data.detail_type,
                "detail_id": detail_data.detail_id,
                "parent_id": detail_data.parent_id,
                "language_id": detail_data.language_id,
                "country_group_id": detail_data.country_group_id,
                "sluggable_text": sluggable_text
            }
        }).responseJSON;
        return newslug;

    };

    this.updateSlugFromSource = function () {

        let value = $(".detail-name", worker.capsule).val();
        let detail_data = worker.pickDetailDataFromCapsule(worker.capsule);
        let old_slug = $(".slug-control", $(worker.capsule)).val();
        let new_slug = worker.getValidSlug(value, detail_data);
        $(".slug-control", $(worker.capsule)).val("/" + new_slug.slug).data("prefix", new_slug.prefix).addClass("focus");

    };

    this.tieHandlers = function () {

        $(".detail-name", worker.capsule).on("change", function (e) {

            let autoupdate = $("input.autoupdate", worker.control).prop("checked");

            if (!autoupdate) {
                return;
            }
            worker.updateSlugFromSource();

        });

        $(".autoupdate", worker.control).on("ifChanged change", function (e) {
            if ($(this).prop("checked")) {
                $(".manual-slug-btn", worker.control).prop("disabled", true);
                worker.updateSlugFromSource();
            } else {
                $(".manual-slug-btn", worker.control).prop("disabled", false);
            }
        });

        $(".manual-slug-btn", worker.control).on("click", function (e) {
            inputValue = $(".slug-control", $(worker.capsule)).val();
            const {value: email} = Swal.fire({
                title: 'Slug ya da slug\'laştırılmak üzere metin girin:',
                input: 'text',
                inputValue: inputValue,
                inputPlaceholder: 'Yeni bir slug değeri belirtin',
                showCancelButton: true,
                cancelButtonColor: '#d33',
                cancelButtonText: 'Vazgeç'
            }).then((result) => {
                if (result.value) {
                    var detail_data = worker.pickDetailDataFromCapsule(worker.capsule);
                    var new_slug = worker.getValidSlug(result.value, detail_data);
                    Swal.fire({
                        title: 'Slug atansın mı?',
                        html:
                          'Girdiğiniz değer: <pre><code>' +
                          JSON.stringify(result.value) +
                          '</code></pre></br>' +
                          ' için uygun slug değeri:' +
                          '<code><pre>/' + new_slug.slug + '</pre></code>',
                        confirmButtonText: 'Onayla',
                        showCancelButton: true,
                        cancelButtonColor: '#d33',
                        cancelButtonText: 'İptal Et'
                    }).then((confirm_question) => {
                        if (confirm_question.value) {
                            $(".slug-control", $(worker.capsule)).val("/" + new_slug.slug).addClass("focus");
                        }
                    });
                }
            });
        });

        $(".sync-slug-btn", worker.control).on("click", function () {
            worker.updateSlugFromSource();
        });

    };

    return worker.init();


};


const MetaWorker = function ($params) {

    const worker = this;

    let default_params = {
        capsule: null,
        url: "",
        details_engine: null
    };

    this.params = $.extend(default_params, $params);


    this.storage = {
        "detail_data": {
            "detail_type": null,
            "detail_id": null,
            "parent_id": null,
            "language_id": null,
            "country_group_id": null,
            "manual_meta": null,
        },
        "manual_meta": null,
        "meta_variables": {},
        "meta_templates": {},
        "realtime_meta_variables": {},
        "realtime_meta_templates": {},
        "extracted_meta_variables": {},
        "flat_meta_variables": {},
        "meta_dom": null,
    };

    this.setManualMeta = function (value) {
        const old_value = worker.storage.manual_meta;
        worker.storage.manual_meta = value;
        if (old_value !== value) {
            worker.metaModChanged(old_value, value);
        }
        return worker.storage.manual_meta;
    };
    this.getManualMeta = function () {
        return worker.storage.manual_meta;
    };

    this.setRealtimeMetaVariable = function (varkey, value) {
        worker.storage.realtime_meta_variables[varkey] = value;
    };
    this.getRealtimeMetaVariable = function (varkey) {
        return typeof worker.storage.realtime_meta_variables[varkey] == "undefined" || worker.storage.realtime_meta_variables[varkey];
    };

    this.setRealtimeMetaTemplate = function (key, value) {
        worker.storage.realtime_meta_templates[key] = value;
    };
    this.getRealtimeMetaTemplate = function (key) {
        return typeof worker.storage.realtime_meta_templates[key] == "undefined" || worker.storage.realtime_meta_templates[key];
    };

    this.setMetaDom = function (value) {
        worker.storage.meta_dom = value;
    };
    this.getMetaDom = function () {
        return worker.storage.meta_dom;
    };

    this.setDetailData = function (value) {
        worker.storage.detail_data = value;
    };
    this.getDetailData = function () {
        return worker.storage.detail_data;
    };




    this.capsule = null;
    this.setCapsule = function (capsule) {
        console.log(capsule);
        worker.capsule = capsule;
    };
    this.getCapsule = function () {
        return $(worker.capsule);
    };

    this.url = null;
    this.setUrl = function (url) {
        worker.url = url;
    };
    this.getUrl = function () {
        return worker.url;
    };

    this.engine = null;
    this.setEngine = function (engine) {
        worker.engine = engine;
    };
    this.getEngine = function () {
        return worker.engine;
    };

    this.control = null;

    this.init = function () {
        worker.setCapsule($(worker.params.capsule));
        //worker.control = $(".detail-slug-control", worker.capsule);
        worker.setUrl(worker.params.url);
        worker.setEngine(worker.params.details_engine);
        worker.helpers.pickDetailDataFromCapsule();
        worker.storage.meta_variables = $(worker.capsule).data("meta-variables");
        worker.storage.meta_templates = $(worker.capsule).data("meta-templates");
        worker.helpers.detectMetaDOM();
        worker.helpers.extractMetaVariables();
        worker.tieHandlers();
        worker.setManualMeta($(".meta-mode-toggler", worker.storage.meta_dom).val());
        worker.previewSeo();
        return worker;
    };


    this.previewSeo = function () {
        let templates = null;
        if (worker.getManualMeta() === "1") {
            templates = worker.storage.realtime_meta_templates;
        } else {
            templates = worker.storage.meta_templates;
        }

        let title_resolved = worker.resolveTemplate(templates.title);
        let description_resolved = worker.resolveTemplate(templates.description);

        $(".seo-preview-title > i").html(title_resolved);
        $(".seo-preview-title > span").text("url burada olacak");
        $(".seo-preview-title").siblings("p").html(description_resolved);
    };

    this.metaModChanged = function (old_value, new_value) {
        if(new_value==1){
            $("input.meta-text-input", worker.storage.meta_dom).change();
        }
        worker.previewSeo();
    };

    this.resolveTemplate = function (template) {

        if( ! template){
            return "";
        }
        RegExp.quote = function(str) {
            return str.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");
        };
        let regex = null;
        $.each(worker.storage.realtime_meta_variables,function(key,value){
            regex ='<'+ key + '>';
            var regobj = new RegExp(RegExp.quote(regex),"g");
            template = template.replace(regobj,value);
        });
        $.each(worker.storage.flat_meta_variables,function(key,value){
            regex ='<'+ key + '>';
            var regobj = new RegExp(RegExp.quote(regex),"g");
            template = template.replace(regobj,value);
        });

        return template;


        //return "resolved text for template: " + template + " " + Math.random();
    };

    this.helpers = {

        "extractMetaVariables": function () {

            const variables = {};
            const flat_variables = {};
            $.each(worker.storage.meta_variables, function (metagroupind, metagroup) {
                $.each(metagroup.variables, function (varname, variable) {
                    variables[varname] = variable;
                    flat_variables[varname] = variable.value;
                });
            });
            worker.storage.extracted_meta_variables = variables;
            worker.storage.flat_meta_variables = flat_variables;

        },

        "pickRealtimeMetaTemplates": function () {
            const mmc = {
                "title": "",
                "title_mobile": "",
                "description": "",
                "keywords": ""
            };

            if (worker.storage.meta_dom.length) {
                let field = $(".meta-title", worker.storage.meta_dom);
                mmc.title = field.length ? field.val() : null;
                field = $(".meta-title-mobile", worker.storage.meta_dom);
                mmc.title_mobile = field.length ? field.val() : null;
                field = $(".meta-description", worker.storage.meta_dom);
                mmc.description = field.length ? field.val() : null;
                field = $(".meta-keywords", worker.storage.meta_dom);
                mmc.keywords = field.length ? field.val() : null;
            }
            worker.storage.realtime_meta_templates = mmc;
        },

        "detectMetaDOM": function () {
            let classname = worker.storage.detail_data.detail_type.replace(/\\/g, '\\\\');
            let dom = $('.detail-metas-control[data-detail-type="' + classname + '"][data-detail-id="' + worker.storage.detail_data.detail_id + '"]');
            worker.setManualMeta($(".meta-mode-toggler", dom).val());
            worker.setMetaDom(dom);
        },

        "pickDetailDataFromCapsule": function () {
            worker.setDetailData({
                "detail_type": $(worker.capsule).data("detail-type"),
                "detail_id": $(worker.capsule).data("detail-id"),
                "parent_type": $(worker.capsule).data("parent-type"),
                "parent_id": $(worker.capsule).data("parent-id"),
                "language_id": $(worker.capsule).data("language-id"),
                "country_group_id": $(worker.capsule).data("country-group-id")
            });
        }

    };


    this.tieHandlers = function () {
        //alert("tying handlers");
        $(".detail-name", worker.capsule).on("change", function (e) {
            //console.log($('.seo-preview-box .seo-preview-title>i', worker.storage.meta_dom).length);
            $('.seo-preview-box .seo-preview-title>i', worker.storage.meta_dom).html($(this).val());

            let dt = worker.storage.detail_data.detail_type;
            let recognized_key = null;

            if (dt === "Mediapress\\Modules\\Content\\Models\\PageDetail") {
                recognized_key = "page_title";
            } else if (dt === "Mediapress\\Modules\\Content\\Models\\SitemapDetail") {
                recognized_key = "sitemap_title";
            } else if (dt === "Mediapress\\Modules\\Content\\Models\\CategoryDetail") {
                recognized_key = "category_title";
            }

            if (recognized_key !== null) {
                worker.setRealtimeMetaVariable(recognized_key, $(this).val());
            }
        });

        $(".detail-detail", worker.capsule).on("change", function (e) {

            $('.seo-preview-box .seo-preview-title>i', worker.storage.meta_dom).html($(this).val());

            let dt = worker.storage.detail_data.detail_type;
            let recognized_key = null;

            if (dt === "Mediapress\\Modules\\Content\\Models\\PageDetail") {
                recognized_key = "page_detail";
            } else if (dt === "Mediapress\\Modules\\Content\\Models\\SitemapDetail") {
                recognized_key = "sitemap_detail";
            } else if (dt === "Mediapress\\Modules\\Content\\Models\\CategoryDetail") {
                recognized_key = "category_detail";
            }

            if (recognized_key !== null) {
                worker.setRealtimeMetaVariable(recognized_key, $(this).val());
            }
        });

        $(".meta-mode-toggler", worker.storage.meta_dom).change(function () {
            worker.setManualMeta($(this).val());
            worker.previewSeo();
            /*console.log(worker.storage.flat_meta_variables);
            console.log(worker.storage.realtime_meta_variables);
            console.log(worker.storage.meta_templates);
            console.log(worker.storage.realtime_meta_templates);*/
        });


        $("input.meta-text-input", worker.storage.meta_dom).change(function () {
            worker.setRealtimeMetaTemplate($(this).data("template-key"),$(this).val());
            worker.previewSeo();
        });
        $(".meta-mode-toggler", worker.storage.meta_dom).change();

    };

    return worker.init();

};