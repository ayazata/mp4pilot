$(document).ready(function () {
    let list = $('.mfile');


    $.each(list, function (i, index) {
        let input = $(list[i]);
        let editor = new mFile(input);

        //editor.build()
    })
});

let mFile = function (htmlObject) {

    let root = this;

    let input = null;

    let id = null;

    let tags = null;

    let name = null;

    let values = null;

    let holder = null;


    this.construct = function (htmlObject) {
        this.input = htmlObject;
        this.id = htmlObject.attr('id');
        this.tags = htmlObject.data('tags');
        this.name = htmlObject.attr('name');
        this.values = JSON.parse(htmlObject.val());
        $('<div id="' + this.id + '" class="upload-tab form-group col col-12"></div>').insertBefore(this.input);
        this.holder = $('#' + this.id);
        this.input.remove();

        $.each(this.tags, function (key, values) {
            root.buildParts(key, values, root.name);
        });
    };

    this.buildParts = function (key, values, name) {

        let element_id = root.generate_id();
        let selection = root.getValues(key);


        let output = '' +
            '<div class="form-group col col-12 post-file" id="' + element_id + '" ata-key="' + key + '" data-options=\'' + root.json(values) + '\'>\n' +
            '    <b>' + values.title + ' <a href="javascript:void(0);" onclick="javascript:addImage($(this))" class="btn btn-primary" role="button">Ekle </a></b>\n' +
            '    <sub>En fazla ' + values.files_max + ' adet yüklenebilir,';
        if (values.filesize_max) {
            output += 'max dosya boyutu ' + values.filesize_max + ' KB, ';
        }

        output += 'kabul edilen formatlar: ' + values.extensions + '</sub>\n' +
            '  <input name="' + name + '->syncMFiles[' + key + ']" type="hidden" value="' + selection + '">\n' +
            '    <div class="slidepage row mb0 upload-list">\n' +
            '        <ul class="sortable">\n' +

            '        </ul>\n' +
            '    </div>\n' +
            '</div>';
        this.holder.append(output);

        handleFileManagerSelection(element_id, JSON.parse(selection));
    };

    this.getValues = function (key) {

        if (root.values[key] != undefined) {
            return root.json(root.values[key]);
        }
        return root.json([]);
    };

    this.json = function (object) {
        let string = JSON.stringify(object);

        return string.replace("/\"/g", '&quot;');
    };

    this.generate_id = function () {
        //edit the token allowed characters
        var a = "abcdef1234567890".split("");
        var b = [];
        for (var i = 0; i < 11; i++) {
            var j = (Math.random() * (a.length - 1)).toFixed(0);
            b[i] = a[j];
        }
        return "mf" + b.join("");
    };
    this.construct(htmlObject);

};

function insertFiles(data, element_id) {


    $.each(data, function (i, value) {
        console.log(value);
        let imgsrc = '';
        if (value.type == 'image') {
            imgsrc = value.url;
        } else {
            imgsrc = 'https://visualpharm.com/assets/369/Image%20File-595b40b85ba036ed117dc1b3.svg';
        }
        let html =
            ' <li class="ui-state-default data-holder" data-id="' + value.id + '">\n' +
            '    <div class="images">\n' +
            '        <img src="' + imgsrc + '" alt="">\n' +
            '        <u>' + value.name + '</u>\n' +
            '    </div>\n' +
            '    <div class="option">\n' +
            '        <span onclick="optionUp($(this));"><img src="/vendor/mediapress/images/option.png" alt=""></span>\n' +
            '        <ul  style="display: none">\n' +
            '            <li><a target="_blank" href="' + imgsrc + '"> <img src="/vendor/mediapress/images/gor.png" alt="">Görüntüle </a></li>\n' +
            '            <li><a href="javascript:void(0);"> <img src="/vendor/mediapress/images/duzenle.png" alt="">Düzenle </a></li>\n' +
            '            <li><a href="javascript:void(0);"> <img src="/vendor/mediapress/images/kirp.png" alt="">Kırp </a></li>\n' +
            '            <li><a href="javascript:void(0);" onclick="javascript:removeImage($(this))"> <img src="/vendor/mediapress/images/sil.png" alt="">Sil </a></li>\n' +
            '        </ul>\n' +
            '    </div>\n' +
            '</li>';

        let sortable = $('#' + element_id).find('.upload-list > ul');
        sortable.append(html);
    });
    $('#' + element_id + ' .sortable').sortable({
        update: function (event, ui) {
            setImages(element_id);
        }
    });
    setImages(element_id);


}

function optionUp(el) {
   el.parent().find('ul').slideToggle();
}

function setImages(element_id) {

    let obj = [];
    $.each($('#' + element_id + ' .sortable > li'), function (i, li) {
        obj.push($(li).data('id'));
    });

    $('#' + element_id + ' input[type=hidden]').val(JSON.stringify(obj));
    $('#' + element_id + ' input[type=hidden]').value = JSON.stringify(obj);
}

function removeImage(el) {

    swal({
        title: 'Emin misiniz?',
        text: 'Dikkat bu işlem sayfayı kayıt ederseniz geri alınamaz!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Evet',
        cancelButtonText: 'Vazgeç',
        closeOnConfirm: false,
        closeOnCancel: false
    }).then(function (isConfirm) {
        if (isConfirm.value) {
            let holder = el.parents('.data-holder');
            let id = holder.data('id');
            var wrapper = el.closest(".post-file");
            var current_values = JSON.parse($('#' + wrapper.attr("id") + ' input[type=hidden]').val());

            current_values = jQuery.grep(current_values, function (value) {
                return value != id;
            });
              $('#' + wrapper.attr("id") + ' input[type=hidden]').val(JSON.stringify(current_values));
            $('#' + wrapper.attr("id") + ' input[type=hidden]').value = JSON.stringify(current_values);
            holder.remove();
        }
    });


}