+ function(t) {
    t.fn.extend({
        resimatt: function() {
            t(this).each(function() {
                var e = t(this).attr("src"),
                    i = t(this).attr("class"),
                    s = t(this).attr("alt");
                (i = "undefined") && (i = "image"), t(this).before('<div class="resim-att"><div class="resim-att-pre"><div class="resim-thumb"><div class="resim-centered"><img src="' + e + '" alt="' + s + '" class="' + i + '-att" /></div></div></div></div>'), t(this).remove()
            })
        }
    })
}(jQuery);

function resim_sil(id) {
    var yes = confirm('Bu resmi silmek istediğinize emin misiniz?');
}
function switchVerticalTab (evt, cityName) {
    var i, tabcontent, tablinks;

    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}
function tableImage() {
    setTimeout(function () {
        $('.lineImage').on("mouseenter", function () {
            var image = $(this).attr('data-image');
            var imgw = $(this).attr('data-width');
            var imgh = $(this).attr('data-height');
            $('body').parent().append("<div class='orjgenel'><div class='row'><div class='orj'><img id='lineImg' src=" + image + " /></div></div></div>");
            var mleft = imgw / 2;
            var mtop = imgh / 2;
            $(".orjgenel").css({"height": imgh, "width": imgw, "margin-left": -mleft, "margin-top": -mtop});
        });
        $(".lineImage").on("mouseleave", function () {
            $(".orjgenel").remove();
        });
    }, 500)
}

$(document).ready(function() {
    tableImage();
    $( ".sortable" ).sortable();
    $( ".sortable" ).disableSelection();

    $('.checkbox').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue'
    });

    $('.langbox ul li a').click(function () {
        $(this).parent().find('ul').slideToggle();
    });

    $('.onn-off').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue'
    });


    $('.onn-off label input').on('ifChecked', function () {
        $(this).parent().parent().parent().next().addClass('on').show().val('').prop('disabled', true);
    });

    $('.onn-off label input').on('ifUnchecked', function () {
        $(this).parent().parent().parent().next().removeClass('on').hide().val('').prop('disabled', false);
    });
    

    $("header .menu .left ul li").on("mouseover", function ()
    {
        var ha = $(this).find('.submenu').height();
        $('i.bg').height(ha+65);
        $(this).find('.submenu').stop().fadeIn(750);
        $(this).addClass('hover');
    });

    $("header .menu .left ul li").on("mouseleave", function ()
    {
        $(this).removeClass('hover');
        $('i.bg').css('height','0');
        $(this).find('.submenu').stop().fadeOut(100);
    });

    var sites = $('.sites ul li > a');
    sites.click(function ()
    {
        if ($(this).parent().hasClass('open'))
        {
            $(this).next().slideUp();
            $(this).parent().removeClass('open');
            $(this).prev().removeClass('open');
        } else
        {
            $('header .menu .right .author ul li').removeClass('open');
            $('header .menu .right .author ul li > ul').slideUp();
            sites.next().slideUp();
            sites.parent().removeClass('open');
            sites.prev().removeClass('open');
            $(this).parent().addClass('open');
            $(this).prev().addClass('open');
            $(this).next().slideDown();
        }
    });


    var aut = $('.author ul li > a');
    aut.click(function ()
    {
        if ($(this).parent().hasClass('open'))
        {
            $(this).next().slideUp();
            $(this).parent().removeClass('open');
            $(this).prev().removeClass('open');
        } else
        {
            $('header .menu .right .sites ul li').removeClass('open');
            $('header .menu .right .sites ul li > ul').slideUp();
            aut.next().slideUp();
            aut.parent().removeClass('open');
            aut.prev().removeClass('open');
            $(this).parent().addClass('open');
            $(this).prev().addClass('open');
            $(this).next().slideDown();
        }
    });

    $("header .menu .left ul li:first-child").on("mouseover", function ()
    {
        $('header i.bg').height(0);
    });
    var pleft = $('.pass-box.left .checkbox label');
    pleft.click(function () {
        if ($(this).hasClass('open')) {
            $(this).removeClass('open');
            $('.form-group.form1').hide();
        } else {
            pleft.parent().removeClass('open');
            pleft.prev().removeClass('open');
            $(this).addClass('open');
            $('.form-group.form1').show();
        }
    });

    $('.form-group').click(function () {
        $(this).addClass('focus');
        $(this).find('input,textarea').focus();
    });

    $('form .form-group input').focus(function(){
        $(this).parent().addClass('focus');
    });


    $('.resim-att').resimatt();

    $(".pass-box li:nth-child(2)").click(function () {
        $(".pass-box .form-group:nth-child(3)").slideDown();
    });
    $(".pass-box li:nth-child(1)").click(function () {
        $(".pass-box .form-group:nth-child(3)").slideUp();
    });



    /*$('.pass-box.right .checkbox label').click(function () {
        $('.datepicks').slideUp();
    });

    $('.pass-box.right #dates label').click(function () {
        $('.datepicks').slideDown();
    });*/


    $('.pass-box.right #dates input').on('ifChecked', function () {
        $('.datepicks').stop().show();
    });

    $('.pass-box.right #dates input').on('ifUnchecked', function () {
        $('.datepicks').stop().hide();
    });

    $("#select1").change(function () {
        if($(this).children(":selected").attr("id") == "sec1") {
            $("#select2").hide();
        }else {
            $("#select2").show();
        }
    });

    $( ".all.url input[type='text']" ).on('keyup',function() {
        var chn= $(this ).val();
        $('.boxes h2 a span').html(chn);
    });
    $( ".all .title input[type='text']" ).on('keyup',function() {
        var chn= $(this ).val();
        $(this).parent().parent().parent().find('.boxes i').html(chn);
    });
    $( ".all .desc textarea" ).on('keyup',function() {
        var chn= $(this ).val();
        $(this).parent().parent().parent().find('.boxes p').html(chn);
    });
    $( ".all .description input[type='text']" ).on('keyup',function() {
        var chn= $(this ).val();
        $(this).parent().parent().parent().find('.boxes p').html(chn);
    });

    $('#nestable').nestable({
        group: 0
    });

    /*var book_name = $('input').val();
    if((book_name == "")||(book_name == null)){
        alert('fail');
    }
    else{
        alert(book_name);
    }*/

    $('.option span').click(function () {
        $(this).parent().find('ul').slideToggle();
    });
    $('.on-off ul li:last-child').click(function () {
        $(this).prev().find('.switch-toggle').addClass('on');
        $(this).prev().find('.switch-toggle').removeClass('off');
        $(this).prev().find("input").val($(this).prev().find('.switch-toggle').data('on')).prop("checked",true).trigger("change");
    });
    $('.option ul li a').click(function () {
        $('.option ul').slideUp();
    });
    $( ".datepick" ).datepicker({
        language: 'tr',
        format:"dd/mm/yyyy",
        dateFormat: "dd/mm/yy",
        altFormat: "yy-mm-dd",
        altField:"#tarih-db",
        monthNames: [ "Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık" ],
        dayNamesMin: [ "Pa", "Pt", "Sl", "Çrş", "Pe", "Cu", "Ct" ],
        firstDay:1
    });

    $( ".url input[type='text']" ).on('keyup',function() {
        var chn= $(this ).val();
        $('.boxes-pro h2 a span').html(chn);
    });
    $( " .title input[type='text']" ).on('keyup',function() {
        var chn= $(this ).val();
        $('.boxes-pro i').html(chn);
    });
    $( ".desc textarea" ).on('keyup',function() {
        var chn= $(this ).val();
        $('.boxes-pro p').html(chn);
    });
    $( ".description input[type='text']" ).on('keyup',function() {
        var chn= $(this ).val();
        $('.boxes-pro p').html(chn);
    });

    $('.on-off ul li:last-child').click(function () {
        $(this).prev().find('.switch-toggle').addClass('on');
        $(this).prev().find('.switch-toggle').removeClass('off');
        $(this).prev().find("input").val($(this).prev().find('.switch-toggle').data('on')).prop("checked",true).trigger("change");
    });
    $('.on-off ul li:first-child').click(function () {
        $(this).next().find('.switch-toggle').addClass('off');
        $(this).next().find('.switch-toggle').removeClass('on');
        $(this).next().find("input").val($(this).next().find('.switch-toggle').data('off')).prop("checked",true).trigger("change");
    });

    $('.on-off.cat  ul li:last-child').click(function () {
        $('.similar').show();
        $('.url-detail').show();
    });
    $('.on-off.cat  ul li:first-child span').click(function () {
        $('.similar').hide();
        $('.url-detail').hide();
    });

    $('.on-off.lang ul li:last-child').click(function () {
        $('.url-detail').show();
        $('.url-detail-lang').show();
    });
    $('.on-off.lang ul li:first-child span').click(function () {
        $('.url-detail').hide();
        $('.url-detail-lang').hide();
    });

    $('.similar .item .plus-button').click(function () {
        $(".similar .item:first-child" ).clone().prependTo( ".similar" ).addClass("delete").removeClass('add');
        $('.similar .delete .plus-button').click(function () {
            $(this).parent().remove();
        });
    });

    $('.on-off.seo  ul li:last-child').click(function () {
        $('.accordion').show();
    });
    $('.on-off.seo  ul li:first-child span').click(function () {
        $('.accordion').hide();
    });

    $('.on-off.url  ul li:last-child').click(function () {
        $('.hidden').show();
    });
    $('.on-off.url  ul li:first-child span').click(function () {
        $('.hidden').hide();
    });

    $('.dropify').dropify();

    $( ".lang-box .form-group textarea" ).on('keyup',function() {
        var ssss = $(this).val().length;
        $(this).parent().find('i.note').html(ssss);
    });

    $( ".lang-box .form-group:first-child input" ).on('keyup',function() {
        var wwww = $(this).val().length;
        $(this).parent().find('i.note').html(wwww);
    });


    $('.pop .item:first-child').addClass('open');
    var popupc = $('.pop h3');
    popupc.click(function ()
    {
        if ($(this).parent().hasClass('open'))
        {
            $(this).next().slideUp();
            $(this).parent().removeClass('open');
            $(this).prev().removeClass('open');
        } else
        {
            popupc.next().slideUp();
            popupc.parent().removeClass('open');
            popupc.prev().removeClass('open');
            $(this).parent().addClass('open');
            $(this).next().slideDown();
        }
    });

    $('.lang .img').click(function () {
        $(this).parent().find('ul').slideToggle();
    });

    $('.lang ul li img').click(function () {
       var langimg = $(this).attr('src');
       $(this).parent().parent().parent().find('.img img').attr('src',langimg);
        $('.lang ul').slideUp();
    });

    $('.social-map .plus-button').click(function () {
        $( ".social-map .item:first-child" ).clone().prependTo( ".social-map form" ).addClass("delete").removeClass('add');
        $('.social-map .delete .plus-button').click(function () {
            $(this).parent().remove();
        });
    });



    $('.chosen-select').chosen();

    var a = $('.message-box ul li .row .head');
    a.click(function() {
        if ($(this).parent().hasClass('open')) {
        } else {
            $(this).parent().parent().parent().find('.detail-content').slideToggle();
            $('html, body').animate({
                scrollTop: $('.message-box ul li.open').offset().top-100
            }, 800);
        }
    });
    $(".lang-toggle span").click(function() {
        $(this).parent().find('ul').toggle();
    });
    $(".lang-toggle ul li a").click(function() {
        $('.lang-toggle ul li').removeClass('active');
        $(this).parent().addClass('active');
        var text = $(this).html();
       /* $(this).parent().parent().html(text);
        $(this).parent().parent().hide();*/
        $(this).parent().parent().parent().find('span').html(text);
        $(this).parent().parent().hide();
    });


    var pana = $('.lang-box .item button');
    pana.click(function () {
        if ($(this).parent().parent().parent().hasClass('open')) {
            $(this).parent().parent().parent().removeClass('open');
            $(this).prev().removeClass('open');
            $(this).parent().parent().parent().find('.edit-center').slideUp();
            $(this).parent().parent().parent().find('.text').slideUp();
            $(this).parent().parent().parent().find('.edit').slideUp();
        } else {
            pana.parent().parent().parent().find('.edit-center').slideUp();
            pana.parent().parent().parent().find('.edit').slideUp();
            pana.parent().parent().parent().find('.text').slideUp();
            pana.parent().parent().parent().removeClass('open');
            pana.prev().removeClass('open');
            $(this).parent().parent().parent().addClass('open');
            $(this).prev().addClass('open');
            $(this).parent().parent().parent().find('.edit-center').slideDown();
            $(this).parent().parent().parent().find('.text').slideDown();
            $(this).parent().parent().parent().find('.edit').slideDown();
        }
    });

    $('.radio label:nth-child(1)').click(function () {
        $(this).parent().parent().next().addClass('hidden');
    });
    $('.radio label:nth-child(3)').click(function () {
        $(this).parent().parent().next().addClass('hidden');
    });
    $('.radio label:nth-child(2)').click(function () {
        $(this).parent().parent().next().removeClass('hidden');
    });
    $('.date label:nth-child(1)').click(function () {
        $(this).parent().parent().next().addClass('hidden');
    });
    $('.date label:nth-child(2)').click(function () {
        $(this).parent().parent().next().removeClass('hidden');
    });
    $('.time label:nth-child(1)').click(function () {
        $(this).parent().find('input[type="text"]').addClass('hidden');
    });
    $('.time label:nth-child(2)').click(function () {
        $(this).parent().find('input[type="text"]').removeClass('hidden');
    });
});