<?php
    /**
     * Created by PhpStorm.
     * User: eray
     * Date: 02.10.2018
     * Time: 17:00
     */

    namespace App\Modules\Content\AllBuilder\Renderables\Pages;


    use Illuminate\Database\Eloquent\Collection;
    use Mediapress\Foundation\AllBuilder\BuilderRenderable;
    use Mediapress\Modules\Content\Models\Page;
    use Mediapress\Modules\Content\Models\Sitemap;

    class Pages extends BuilderRenderable
    {

        public function defaultContents()
        {

            extract($this->params);

            //dd($page->gender);
            //dd($page_categories);

            //dd($categories);

            return [
                [
                    "type" => "form",
                    "options" => [
                        "html" => [
                            "attributes" => [
                                "method" => "POST",
                                "action" => url(route("Content.pages.update", ["sitemap_id" => $page->sitemap->id, "id" => $page->id]))
                            ]
                        ],
                    ],
                    "contents" => [
                        [
                            "type" => "steptabs",
                            "contents" => [
                                [
                                    "type" => "tab",
                                    "options" => [
                                        "title" => "1. Genel",
                                        "navigation" => true
                                    ],
                                    "contents" => [
                                        [
                                            "type" => "div",
                                            "options" => [
                                                "html" => [
                                                    "attributes" => [
                                                        "class" => "contents"
                                                    ]
                                                ]
                                            ],
                                            "contents" => [

                                                [
                                                    "type" => "pagecategoriescontrol",
                                                    "params" => [
                                                        "page_model" => $page,
                                                    ],
                                                    "options" => [
                                                        "html" => [
                                                            "attributes" => [
                                                                "class" => "multiple",
                                                                "multiple" => "multiple",
                                                            ]
                                                        ],
                                                        "multiple" => true,
                                                        "caption_mode" => "last_two",
                                                        "show_default_option" => true
                                                    ]
                                                ],
                                                /*[
                                                    "type" => "selectwithlabel",
                                                    "options" => [
                                                        "title" => "Kategori Seçin",
                                                        "html" => [
                                                            "attributes" => [
                                                                "class" => "multiple",
                                                                "multiple" => "multiple",
                                                                "name" => "page->" . $page->id . "->sync:categories[]"
                                                            ]
                                                        ],
                                                        "show_default_option" => false,
                                                        "value" => $page_categories
                                                    ],
                                                    "data" => [
                                                        "values" => $sitemap_categories*/ /*[
                                                            "type" => "function_on_item",
                                                            "item" => $sitemap_categories,//<var>page->sitemap->categories</var>
                                                            "function" => "pluck",
                                                            "params" => [
                                                                "name",
                                                                "id"
                                                            ]
                                                        ]*//*,
                                                    ]
                                                ],*/

                                                [
                                                    "type" => "detailtabs",
                                                    "params" => [
                                                        "details" => "<var>page->details</var>",
                                                    ],
                                                    "options" => [
                                                        "capsulate" => true,
                                                        "meta_variables"=>true,
                                                        "html" => [
                                                            "attributes" => [
                                                                "class" => "tab-list"
                                                            ]
                                                        ]
                                                    ],
                                                    "contents" => [

                                                        "tab" => [
                                                            "type" => "tab",
                                                            "contents" => [
                                                                [
                                                                    "type" => "inputwithlabel",
                                                                    "options" => [
                                                                        "rules" => "",
                                                                        "title" => "Başlık",
                                                                        "html" => [
                                                                            "attributes" => [
                                                                                "name" => "detail->name",
                                                                                "value" => "<print>detail->name</print>",
                                                                                "class" => "detail-name"
                                                                            ]
                                                                        ]
                                                                    ]
                                                                ],
                                                                [
                                                                    "type" => "detailslugcontrol",
                                                                    "options" => [
                                                                        "detail" => "<var>detail</var>",
                                                                        "html" => [
                                                                            "attributes" => [
                                                                                "name" => "detail->slug",
                                                                                "value" => "<print>detail->slug</print>"
                                                                            ]
                                                                        ]
                                                                    ]
                                                                ],
                                                                [
                                                                    "type" => "textareawithlabel",
                                                                    "options" => [
                                                                        "rules" => "",
                                                                        "value" => "<print>detail->name</print>",
                                                                        "title" => "Ayrıntılı Bilgi",
                                                                        "html" => [
                                                                            "attributes" => [
                                                                                "name" => "detail->detail",
                                                                                "class" => "ckeditor detail-detail",
                                                                                "id" => "editor_<print>detail->id</print>"
                                                                            ]
                                                                        ]
                                                                    ],
                                                                    "data" => [
                                                                        "stacks" => [
                                                                            "scripts" => '<script> $(document).ready(function(){CKEDITOR.replace("editor_<print>detail->id</print>")});</script>'
                                                                        ]
                                                                    ]
                                                                ],
                                                                [
                                                                    "type" => "formgroup",
                                                                    "contents" => [
                                                                        [
                                                                            "type" => "icheck",
                                                                            "options" => [
                                                                                "title" => "Doğru mu? <print>detail->page->sitemap->detail->name</print>",
                                                                                "checked_if_value_is" => "<print>detail->dogru_mu</print>",
                                                                                "html" => [
                                                                                    "attributes" => [
                                                                                        "name" => "detail->extras->dogru_mu",
                                                                                        "class" => "form-check-input",
                                                                                        "value" => "1",
                                                                                        "type" => "checkbox"
                                                                                    ]
                                                                                ],
                                                                            ]
                                                                        ]
                                                                    ]

                                                                ],
                                                                [
                                                                    "type" => "clearfix"
                                                                ]
                                                            ]
                                                        ]

                                                    ]
                                                ],

                                            ]
                                        ],

                                    ]
                                ],
                                [
                                    "type" => "tab",
                                    "options" => [
                                        "title" => "2. Fotoğraflar"
                                    ],
                                    "contents" => [
                                        [
                                            "type" => "div",
                                            "options" => [
                                                "html" => [
                                                    "attributes" => [
                                                        "class" => "contents"
                                                    ]
                                                ]
                                            ],
                                            "contents" => [
                                                "Burada çıkacak",
                                                [
                                                    'type' => 'mpfile',
                                                    'options' =>
                                                        [
                                                            'tags' => ['cover' => '{%quot%key%quot%:%quot%cover%quot%,%quot%file_type%quot%:%quot%image%quot%,%quot%required%quot%:%quot%required%quot%,%quot%title%quot%:%quot%Kapak Fotoğrafı%quot%,%quot%allow_actions%quot%:[%quot%select%quot%,%quot%upload%quot%],%quot%allow_diskkeys%quot%:[%quot%azure%quot%,%quot%local%quot%],%quot%extensions%quot%:%quot%JPG,PNG,GIF%quot%,%quot%pixels_x_min%quot%:%quot%%quot%,%quot%pixels_x_max%quot%:%quot%%quot%,%quot%pixels_y_min%quot%:%quot%%quot%,%quot%pixels_y_max%quot%:%quot%%quot%,%quot%pixels_x%quot%:%quot%%quot%,%quot%pixels_y%quot%:%quot%%quot%,%quot%filesize_min%quot%:%quot%%quot%,%quot%filesize_max%quot%:%quot%4096%quot%,%quot%files_max%quot%:%quot%1%quot%,%quot%additional_rules%quot%:%quot%%quot%}',
                                                            ],
                                                            "html" => [
                                                                "attributes" => [
                                                                    "name" => "page-><print>page->id</print>"
                                                                ]
                                                            ]
                                                        ],
                                                    "params" => [
                                                        "files" => "<var>page->mfiles</var>"
                                                    ]
                                                ],
                                            ]
                                        ],
                                    ]
                                ],
                                [
                                    "type" => "tab",
                                    "options" => [
                                        "title" => "3. Benzer Ürünler"
                                    ],
                                    "contents" => [
                                        [
                                            "type" => "div",
                                            "options" => [
                                                "html" => [
                                                    "attributes" => [
                                                        "class" => "contents"
                                                    ]
                                                ]
                                            ],
                                            "contents" => [

                                            ]
                                        ],
                                    ]
                                ],
                                [
                                    "type" => "tab",
                                    "options" => [
                                        "title" => "4. Kriter & Özellik"
                                    ],
                                    "contents" => [
                                        [
                                            "type" => "div",
                                            "options" => [
                                                "html" => [
                                                    "attributes" => [
                                                        "class" => "contents"
                                                    ]
                                                ]
                                            ],
                                            "contents" => [
                                                [
                                                    "type" => "simpletabs",
                                                    "contents" => [
                                                        "criterias_tab" => [
                                                            "type" => "tab",
                                                            "options" => [
                                                                "title" => "Kriterler"
                                                            ],
                                                            "contents" => [
                                                                /*[
                                                                    "type" => "pagecriteriascontrol",
                                                                    "params" => [
                                                                        "page_model" => $page
                                                                    ]
                                                                ],*/
                                                            ]
                                                        ],
                                                        "properties_tab" => [
                                                            "type" => "tab",
                                                            "options" => [
                                                                "title" => "Özellikler"
                                                            ],
                                                            "contents" => [
                                                                /*[
                                                                    "type"=>"pagepropertiescontrol",
                                                                    "params"=>[
                                                                        "page_model"=>$page
                                                                    ]
                                                                ]*/
                                                            ]
                                                        ]
                                                    ]
                                                ]
                                            ]
                                        ],
                                    ]
                                ],
                                [
                                    "type" => "tab",
                                    "options" => [
                                        "title" => "5. SEO"
                                    ],
                                    "contents" => [
                                        [
                                            "type" => "div",
                                            "options" => [
                                                "html" => [
                                                    "attributes" => [
                                                        "class" => "contents"
                                                    ]
                                                ]
                                            ],
                                            "contents" => [
                                                "detail_tabs" => [
                                                    "type" => "detailtabs",
                                                    "params" => [
                                                        "details" => "<var>page->details</var>",
                                                    ],
                                                    "options" => [
                                                        "capsulate" => false,
                                                        "meta_variables"=>false,
                                                        "html" => [
                                                            "attributes" => [
                                                                "class" => "tab-list"
                                                            ]
                                                        ]
                                                    ],
                                                    "contents" => [
                                                        "tab" => [
                                                            "type" => "tab",
                                                            "contents" => [
                                                                "detail_metas_control" => [
                                                                    "type" => "detailmetascontrol",
                                                                    "params"=>[
                                                                        "detail"=>'<var>detail</var>'
                                                                    ]
                                                                ]
                                                            ]
                                                        ]
                                                    ]
                                                ],

                                            ]
                                        ],
                                    ]
                                ],
                                [
                                    "type" => "tab",
                                    "options" => [
                                        "title" => "6. Yayınla"
                                    ],
                                    "contents" => [
                                        [
                                            "type" => "div",
                                            "options" => [
                                                "html" => [
                                                    "attributes" => [
                                                        "class" => "contents"
                                                    ]
                                                ]
                                            ],
                                            "contents" => [
                                                [
                                                    "type"=>"div",
                                                    "options"=>[
                                                        "html"=>[
                                                            "attributes"=>[
                                                                "class"=>"row"
                                                            ]
                                                        ]
                                                    ],
                                                    "contents"=>[
                                                        /*[
                                                            "type"=>"div",
                                                            "options"=>[
                                                                "html"=>[
                                                                    "attributes"=>[
                                                                        "class"=>"col-6"
                                                                    ]
                                                                ]
                                                            ],
                                                            "contents"=>[
                                                                [
                                                                    "type"=>"div",
                                                                    "options"=>[
                                                                        "attributes"=>[
                                                                            "class"=>"pass-box left"
                                                                        ]
                                                                    ],
                                                                    "contents"=>[
                                                                        "Sol taraf"
                                                                    ]
                                                                ]
                                                            ]
                                                        ],*/
                                                        [
                                                            "type"=>"contentprotectioncontrol",
                                                            "params"=>[
                                                                "object"=>$page
                                                            ]
                                                        ],
                                                        [
                                                            "type"=>"div",
                                                            "options"=>[
                                                                "html"=>[
                                                                    "attributes"=>[
                                                                        "class"=>"col-6"
                                                                    ]
                                                                ]
                                                            ],
                                                            "contents"=>[
                                                                [
                                                                    "type"=>"div",
                                                                    "options"=>[
                                                                        "attributes"=>[
                                                                            "class"=>"pass-box right"
                                                                        ]
                                                                    ],
                                                                    "contents"=>[
                                                                        [
                                                                            "type" => "pagestatuscontrol",
                                                                            "options"=>[
                                                                                "multiline"=>true
                                                                            ],
                                                                            "params" => [
                                                                                "page_model" => $page
                                                                            ]
                                                                        ],
                                                                    ]
                                                                ]
                                                            ]
                                                        ],
                                                    ]
                                                ],


                                            ]
                                        ],
                                    ]
                                ]
                            ]
                        ],
                        [
                            "type" => "button",
                            "options" => [
                                "html" => [
                                    "tag" => "button",
                                    "attributes" => [
                                        "type" => "submit"
                                    ]
                                ],
                                "caption" => "Gönder-",
                                "iconname" => "save",
                                "iconsize" => "1"
                            ]
                        ],
                    ]
                ]

            ];

            /*return [
                [
                    "type" => "blank",
                    "options" => [
                        "html" => [
                            "tag" => "style",
                            "attributes" => [
                                "type" => "text/css"
                            ]
                        ]
                    ],
                    "contents" => [
                        "ul.nav.nav-tabs{display:inline-block;}"
                    ]
                ],
                [
                    "type" => "form",
                    "options" => [
                        "html" => [
                            "attributes" => [
                                "class" => "form-inline",
                                "method" => "POST",
                                "action" => url(route("admin.pages.update", $page->id))
                            ]
                        ],
                        "collectable_as" => ["form", "pagesform"]
                    ],
                    "contents" => [
                        [
                            "type" => "detailtabs",
                            "params" => [
                                "details" => "<var>page->details</var>",
                            ],
                            "contents" => [

                                "tab" => [
                                    "type" => "tab",
                                    "contents" => [
                                        [
                                            "type" => "inputwithlabel",
                                            "options" => [
                                                "rules" => "",
                                                "title" => "Başlık",
                                                "html" => [
                                                    "attributes" => [
                                                        "name" => "detail->name",
                                                        "value" => "<print>detail->name</print>"
                                                    ]
                                                ]
                                            ]
                                        ],
                                        [
                                            "type" => "formgroup",
                                            "contents" => [
                                                [
                                                    "type" => "icheck",
                                                    "options" => [
                                                        "title" => "Doğru mu? <print>detail->page->sitemap->detail->name</print>",
                                                        "checked_if_value_is" => "<print>detail->dogru_mu</print>",
                                                        "html" => [
                                                            "attributes" => [
                                                                "name" => "detail->extras->dogru_mu",
                                                                "class" => "form-check-input",
                                                                "value" => "1",
                                                                "type" => "checkbox"
                                                            ]
                                                        ],
                                                    ]
                                                ]
                                            ]

                                        ],
                                        [
                                            "type" => "clearfix"
                                        ]
                                    ]
                                ],
                                "general" => [
                                    "type" => "tab",
                                    "options" => [
                                        "title" => "Genel"
                                    ],
                                    "contents" => [
                                        [
                                            "type" => "formgroup",
                                            "contents" => [
                                                [
                                                    "type" => "selectwithlabel",
                                                    "options" => [
                                                        "title" => "Kategori Seçin",
                                                        "html" => [
                                                            "attributes" => [
                                                                "class" => "multiple",
                                                                "multiple" => "multiple",
                                                                "name" => "page->" . $page->id . "->sync:categories[]"
                                                            ]
                                                        ],
                                                        "show_default_option" => false,
                                                        "value" => $page_categories
                                                    ],
                                                    "data" => [
                                                        "values" => $sitemap_categories /*[
                                                            "type" => "function_on_item",
                                                            "item" => $sitemap_categories,//<var>page->sitemap->categories</var>
                                                            "function" => "pluck",
                                                            "params" => [
                                                                "name",
                                                                "id"
                                                            ]
                                                        ]*//*,
                                                    ]
                                                ],
                                            ]
                                        ],
                                        [
                                            "type" => "formcheck",
                                            "contents" => [
                                                [
                                                    "type" => "radio",
                                                    "options" => [
                                                        "title" => "Erkek",
                                                        "checked" => [$page->gender, null, "=="],
                                                        "checked_if_value_is" => $page->gender,
                                                        "html" => [
                                                            "attributes" => [
                                                                "name" => "page->" . $page->id . "->extras->gender",
                                                                "value" => "male"
                                                            ]
                                                        ]
                                                    ]
                                                ]
                                            ]
                                        ],
                                        [
                                            "type" => "formcheck",
                                            "contents" => [
                                                [
                                                    "type" => "radio",
                                                    "options" => [
                                                        "title" => "Kadın",
                                                        "checked_if_value_is" => $page->gender,
                                                        "html" => [
                                                            "attributes" => [
                                                                "name" => "page->" . $page->id . "->extras->gender",
                                                                "value" => "female"
                                                            ]
                                                        ]
                                                    ]
                                                ]
                                            ]
                                        ],
                                        [
                                            "type"=>"radiosamigos",
                                            "options"=>[
                                                "value"=>$page->gender,
                                                "default"=>"male",
                                                "name"=>"gender"
                                            ],
                                            "data"=>[
                                                "values"=>[
                                                    "male"=>"Erkek",
                                                    "female"=>"Kadın"
                                                ]
                                            ]
                                        ]
                                    ]
                                ]

                            ]
                        ],
                        [
                            "type" => "button",
                            "options" => [
                                "html" => [
                                    "tag" => "button",
                                    "attributes" => [
                                        "type" => "submit"
                                    ]
                                ]
                            ],
                            "contents" => [
                                "Gönder"
                            ]
                        ]
                    ]
                ]
            ];*/

        }
    }