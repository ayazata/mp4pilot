<?php

namespace App\Modules\Content\AllBuilder\Renderables\Pages;


use Mediapress\Foundation\AllBuilder\BuilderRenderable;

class Properties extends BuilderRenderable
{
    public function defaultContents()
    {
        extract($this->params);
        return [
            [
                "type" => "form",
                "options" => [
                    "html" => [
                        "attributes" => [
                            "method" => "post",
                            "action" => route("admin.properties.update", ["sitemap_id" => $property->sitemap_id, "id" => $property->id]),
                        ]
                    ]
                ],
                "contents" => [
                    [
                        "type" => "steptabs",
                        "contents" => [
                            [
                                "type" => "tab",
                                "options" => [
                                    "title" => "Genel",
                                    "navigation" => true
                                ],
                                "contents" => [
                                    [
                                        "type" => "div",
                                        "options" => [
                                            "html" => [
                                                "attributes" => [
                                                    "class" => "contents"
                                                ]
                                            ]
                                        ],
                                        "contents" => [
                                            [
                                                "type" => "pagestatuscontrol",
                                                "params" => [
                                                    "page_model" => $property,
                                                ]
                                            ],
                                            [
                                                "type" => "detailtabs",
                                                "params" => [
                                                    "details" => "<var>property->details</var>",
                                                ],
                                                "options" => [
                                                    "html" => [
                                                        "attributes" => [
                                                            "class" => "tab-list"
                                                        ]
                                                    ]
                                                ],
                                                "contents" => [
                                                    "tab" => [
                                                        "type" => "tab",
                                                        "contents" => [
                                                            [
                                                                "type" => "inputwithlabel",
                                                                "options" => [
                                                                    "rules" => "",
                                                                    "title" => "Başlık",
                                                                    "html" => [
                                                                        "attributes" => [
                                                                            "name" => "detail->name",
                                                                            "value" => "<print>detail->name</print>"
                                                                        ]
                                                                    ]
                                                                ]
                                                            ],
                                                            [
                                                                "type" => "ckeditor",
                                                                "options" => [
                                                                    "rules" => "",
                                                                    "title" => "Detay Metni",
                                                                    "value" => "<print>detail->detail</print>",
                                                                    "html" => [
                                                                        "attributes" => [
                                                                            "name" => "detail->detail",
                                                                        ]
                                                                    ]
                                                                ]
                                                            ],
                                                            [
                                                                "type" => "clearfix"
                                                            ]
                                                        ]
                                                    ]
                                                ]
                                            ],
                                        ]
                                    ],

                                ]
                            ],

                            [
                                "type" => "tab",
                                "options" => [
                                    "title" => "Yayınla"
                                ],
                                "contents" => [
                                    [
                                        "type" => "div",
                                        "options" => [
                                            "html" => [
                                                "attributes" => [
                                                    "class" => "contents"
                                                ]
                                            ]
                                        ],
                                        "contents" => [
                                            [
                                                "type" => "button",
                                                "options" => [
                                                    "html" => [
                                                        "attributes" => [
                                                            "type" => "submit",
                                                            "class" => "btn btn-primary"
                                                        ]
                                                    ]
                                                ],
                                                "contents" => [
                                                    [
                                                        "type" => "faicon",
                                                        "options" => [
                                                            "icon" => "save"
                                                        ]
                                                    ],
                                                    "Kaydet"
                                                ]
                                            ]
                                        ]
                                    ],
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];

    }
}
