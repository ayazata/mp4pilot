<?php
    /**
     * Created by PhpStorm.
     * User: eray
     * Date: 02.10.2018
     * Time: 17:00
     */

    namespace App\Modules\Content\AllBuilder\Renderables\Products_10;


    use Illuminate\Database\Eloquent\Collection;
    use Mediapress\Foundation\AllBuilder\BuilderRenderable;
    use Mediapress\Modules\Content\Models\Page;
    use Mediapress\Modules\Content\Models\Sitemap;

    class Pages extends BuilderRenderable
    {

        public function defaultContents()
        {

            extract($this->params);

            $sitemap_categories = Sitemap::find(2)->categories()->with(['detail'])->get()->pluck('detail.name', 'id');
            $page_categories = $page->categories->pluck('id')->toArray();

            //dd($page->gender);
            //dd($page_categories);

            //dd($categories);


            return [
                [
                    "type" => "blank",
                    "options" => [
                        "html" => [
                            "tag" => "style",
                            "attributes" => [
                                "type" => "text/css"
                            ]
                        ]
                    ],
                    "contents" => [
                        "ul.nav.nav-tabs{display:inline-block;}"
                    ]
                ],
                [
                    "type" => "form",
                    "options" => [
                        "html" => [
                            "attributes" => [
                                "class" => "form-inline",
                                "method" => "POST",
                                "action" => url(route("admin.pages.update", $page->id))
                            ]
                        ],
                        "collectable_as" => ["form", "pagesform"]
                    ],
                    "contents" => [
                        [
                            "type" => "detailtabs",
                            "params" => [
                                "details" => "<var>page->details</var>",
                            ],
                            "contents" => [

                                "tab" => [
                                    "type" => "tab",
                                    "contents" => [
                                        [
                                            "type" => "inputwithlabel",
                                            "options" => [
                                                "rules" => "",
                                                "title" => "Başlık",
                                                "html" => [
                                                    "attributes" => [
                                                        "name" => "detail->name",
                                                        "value" => "<print>detail->name</print>"
                                                    ]
                                                ]
                                            ]
                                        ],
                                        [
                                            "type" => "formgroup",
                                            "contents" => [
                                                [
                                                    "type" => "icheck",
                                                    "options" => [
                                                        "title" => "Doğru mu? <print>detail->page->sitemap->detail->name</print>",
                                                        "checked_if_value_is" => "<print>detail->dogru_mu</print>",
                                                        "html" => [
                                                            "attributes" => [
                                                                "name" => "detail->extras->dogru_mu",
                                                                "class" => "form-check-input",
                                                                "value" => "1",
                                                                "type" => "checkbox"
                                                            ]
                                                        ],
                                                    ]
                                                ]
                                            ]

                                        ],
                                        [
                                            "type" => "clearfix"
                                        ]
                                    ]
                                ],
                                "general" => [
                                    "type" => "tab",
                                    "options" => [
                                        "title" => "Genel"
                                    ],
                                    "contents" => [
                                        [
                                            "type" => "formgroup",
                                            "contents" => [
                                                [
                                                    "type" => "selectwithlabel",
                                                    "options" => [
                                                        "title" => "Kategori Seçin",
                                                        "html" => [
                                                            "attributes" => [
                                                                "class" => "multiple",
                                                                "multiple" => "multiple",
                                                                "name" => "page->" . $page->id . "->sync:categories[]"
                                                            ]
                                                        ],
                                                        "show_default_option" => false,
                                                        "value" => $page_categories
                                                    ],
                                                    "data" => [
                                                        "values" => $sitemap_categories /*[
                                                            "type" => "function_on_item",
                                                            "item" => $sitemap_categories,//<var>page->sitemap->categories</var>
                                                            "function" => "pluck",
                                                            "params" => [
                                                                "name",
                                                                "id"
                                                            ]
                                                        ]*/,
                                                    ]
                                                ],
                                            ]
                                        ],
                                        [
                                            "type" => "formcheck",
                                            "contents" => [
                                                [
                                                    "type" => "radio",
                                                    "options" => [
                                                        "title" => "Erkek",
                                                        "checked" => [$page->gender, null, "=="],
                                                        "checked_if_value_is" => $page->gender,
                                                        "html" => [
                                                            "attributes" => [
                                                                "name" => "page->" . $page->id . "->extras->gender",
                                                                "value" => "male"
                                                            ]
                                                        ]
                                                    ]
                                                ]
                                            ]
                                        ],
                                        [
                                            "type" => "formcheck",
                                            "contents" => [
                                                [
                                                    "type" => "radio",
                                                    "options" => [
                                                        "title" => "Kadın",
                                                        "checked_if_value_is" => $page->gender,
                                                        "html" => [
                                                            "attributes" => [
                                                                "name" => "page->" . $page->id . "->extras->gender",
                                                                "value" => "female"
                                                            ]
                                                        ]
                                                    ]
                                                ]
                                            ]
                                        ],
                                        [
                                            "type"=>"radiosamigos",
                                            "options"=>[
                                                "value"=>$page->gender,
                                                "default"=>"male",
                                                "name"=>"gender"
                                            ],
                                            "data"=>[
                                                "values"=>[
                                                    "male"=>"Erkek",
                                                    "female"=>"Kadın"
                                                ]
                                            ]
                                        ]
                                    ]
                                ]

                            ]
                        ],
                        [
                            "type" => "button",
                            "options" => [
                                "html" => [
                                    "tag" => "button",
                                    "attributes" => [
                                        "type" => "submit"
                                    ]
                                ]
                            ],
                            "contents" => [
                                "Gönder"
                            ]
                        ]
                    ]
                ]
            ];

        }
    }