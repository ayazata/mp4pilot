<?php
    /**
     * Created by PhpStorm.
     * User: eray
     * Date: 21.09.2018
     * Time: 15:15
     */

    namespace App\BuilderBlueprints;


    use Mediapress\Foundation\FormBuilder\BuilderBase;

    class Sitemap1Details extends BuilderBase
    {

        public function getParseble($params_to_extract = [])
        {
            if ($params_to_extract) {
                extract($params_to_extract);
            } else {
                extract($this->params);
            }

            return
                [
                    "type" => "steptabs",
                    "params" => [],
                    "contents" => [
                        "genel" => [
                            "type" => "tab",
                            "params" => ["caption" => "Genel"],
                            "contents" => [
                                "categories" => [
                                    "type" => "multiselect",
                                    "params" => [
                                        "caption" => "İlgili Kategoriler",
                                        "data" => [
                                            "type" => "datasource",
                                            "params" => [
                                                "name" => "Sitemap1Categories",
                                                "params" => []
                                            ]
                                        ]
                                    ]
                                ],
                                "detailsloop" => [
                                    "type" => "foreach",
                                    "params" => [
                                        "array" => $this->object->details,
                                        "keyname" => "key",
                                        "itemname" => "detail"
                                    ],
                                    "contents" => [

                                    ]
                                ],
                                "vartabs" => [
                                    [
                                        "type" => "tabs",
                                        "params" => [],
                                        "contents" => [

                                        ]
                                    ]
                                ]
                            ]

                        ]
                    ],
                    "initial" => null,
                    "stepwizard" => [
                        "genel" => [
                            "caption" => trans("ContentPanel::general.general")
                        ]
                    ]
                ];
        }


    }