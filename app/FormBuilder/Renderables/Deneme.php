<?php
    /**
     * Created by PhpStorm.
     * User: eray
     * Date: 02.10.2018
     * Time: 17:00
     */

    namespace App\FormBuilder\Renderables;


    use Mediapress\Foundation\FormBuilder\BuilderRenderable;

    class Deneme extends BuilderRenderable
    {

        public function defaultContents()
        {

            extract($this->params);
            //$object->id=469259;

            return [
                [
                    "type"=>"form",
                    "params"=>[
                        "html"=>[
                            "attributes"=>[
                                "class"=>"form-inline",
                                "method"=>"GET",
                                "action"=>url(route("admin.formbuilder.store"))
                            ]
                        ]
                    ],
                    "options"=>[
                        "collectable_as"=>["form", "sitemapform"]
                    ],
                    "contents"=>[
                        [
                            "type"=>"inputwithlabel",
                            "params"=>[
                                "title"=>"Başlık",
                                "html"=>[
                                    "tag"=>"input",
                                    "attributes"=>[
                                        "type"=>"number",
                                        "class"=>"form-control",
                                        "name"=>"sm->new:eray(sitemap_type_id:60,website_id:70)->custom",
                                        "value"=>"<print>sitemap->custom</print>",
                                    ]
                                ],
                                "rules"=>"string"
                            ],
                            "contents"=>[]
                        ],
                        [
                            "type"=>"input",
                            "params"=>[
                                "html"=>[
                                    "tag"=>"input",
                                    "attributes"=>[
                                        "type"=>"text",
                                        "class"=>"form-control",
                                        "name"=>"sm->eray->extras->saban",
                                        "value"=>"<print>sitemap->custom</print>",
                                    ]
                                ],
                                "rules"=>"string"
                            ],
                            "contents"=>[]
                        ],
                        [
                            "type" => "foreach",
                            "data" => [
                                "array" => "<var>sitemap->details</var>",
                                "keyname" => "key",
                                "itemname" => "detail"
                            ],
                            "contents" => [
                                [
                                    "type"=>"if",
                                    "params"=>[
                                        "det"=>"<var>&detail</var>",
                                        "conditions"=>[
                                            "<var>det->id</var>",
                                            10000,
                                            "!=="
                                        ]
                                    ],

                                    "contents"=>[
                                        [
                                            "type"=>"inputwithlabel",
                                            "params"=>[
                                                "title"=>"Detay Adı",
                                                "html"=>[
                                                    "tag"=>"input",
                                                    "attributes"=>[
                                                        "type"=>"text",
                                                        "class"=>"form-control",
                                                        "name"=>"smd-><print>det->id</print>->extras->extradene",
                                                        "value"=>"<print>det->name</print>",
                                                    ]
                                                ],
                                                "rules"=>"string"
                                            ],
                                            "contents"=>[]
                                        ]

                                    ]
                                ],
                                /*[
                                    "type"=>"if",
                                    "params"=>[
                                        "det"=>"<var>&detail</var>",
                                        "conditions"=>[
                                            "<var>det->id</var>",
                                            10000,
                                            "!=="
                                        ]
                                    ],

                                    "contents"=>[
                                        [
                                            "type"=>"input",
                                            "params"=>[
                                                "html"=>[
                                                    "tag"=>"input",
                                                    "attributes"=>[
                                                        "type"=>"text",
                                                        "class"=>"form-control",
                                                        "name"=>"sitemap_detail[<print>det->language->code</print>][<print>det->country_code</print>]",
                                                        "value"=>"<print>det->name</print>",
                                                    ]
                                                ],
                                                "rules"=>"required"
                                            ],
                                            "contents"=>[]
                                        ]

                                    ]
                                ]*/
                            ]
                        ],
                        [
                            "type"=>"button",
                            "params"=>[
                                "html"=>[
                                    "tag"=>"button",
                                    "attributes"=>[
                                        "type"=>"submit"
                                    ]
                                ]
                            ],
                            "contents"=>[
                                "Gönder"
                            ]
                        ]
                    ]
                ]
            ];

        }
    }