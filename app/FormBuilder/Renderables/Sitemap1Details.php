<?php
    /**
     * Created by PhpStorm.
     * User: eray
     * Date: 24.09.2018
     * Time: 16:03
     */

    namespace App\BuilderBlueprints;

    use Mediapress\Foundation\FormBuilder\BuilderRenderable;

    class Sitemap1Details extends BuilderRenderable
    {
        public function defaultContents()
        {
            extract($this->params);
            $deneme = new \stdClass();
            $deneme->ordernum = [1, 2, 3, 4, 5];
            return [
                [
                    "type" => "steptabs",
                    "params" => [],
                    "contents" => [
                        "form" => [
                            "type" => "form",
                            "params" => [
                                "html_attributes" => [
                                    "prepend_csrf_field" => true,
                                    "formstorer" => "sitemapdetails"
                                ]
                            ],
                            "contents" => [
                                "genel"=>[
                                    "type" => "tab",
                                    "params" => [
                                        "caption" => "Genel",
                                    ],
                                    "contents" => [
                                        "categories" => [
                                            "type" => "multiselect",
                                            "params" => [
                                                "habununobjesi" => &$deneme,
                                                "caption" => "İlgili Kategoriler of sitemap(id=<prop>object->id</prop> Tarih=<prop>object->created_at</prop>)",
                                                "caption2" => "<prop>&habununobjesi->ordernum->3</prop>",
                                                "data" => [/*"type" => "datasource", "params" => [ "name" => "Sitemap1Categories", "params" => [] ]*/]
                                            ],
                                            "contents" => []
                                        ],
                                        "detailsloop" => [
                                            "type" => "foreach",
                                            "params" => [
                                                "array" => $object->details,
                                                "keyname" => "key",
                                                "itemname" => "detail"
                                            ],
                                            "contents" => [
                                                "titleinput" => [
                                                    "type" => "textinputgroup",
                                                    "params" => [
                                                        "wrapper"=>[

                                                        ],
                                                        "label"=>[

                                                        ],
                                                        "input"=>[

                                                        ],
                                                        "tag" => "p<prop>detail->name</prop>",
                                                        /*"html_attributes"=>[
                                                            "name"=>"name[<prop>detail->language->code</prop>][<prop>detail->country_code</prop>]"
                                                        ]*/
                                                    ],
                                                    "contents" => [
                                                        //"--prop:detail->name--"
                                                    ]
                                                ]
                                            ]
                                        ],
                                        /*"vartabs" => [
                                                "type" => "detailtabs",
                                                "params" => [
                                                    "array" => $object->details,
                                                ],
                                                "contents" => [

                                                ]
                                         ]*/
                                    ]
                                ]
                            ],
                        ],
                    ]
                ]
            ];
        }


    }

