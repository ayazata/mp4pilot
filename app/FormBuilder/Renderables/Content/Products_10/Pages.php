<?php
    /**
     * Created by PhpStorm.
     * User: eray
     * Date: 02.10.2018
     * Time: 17:00
     */

    namespace App\FormBuilder\Renderables\Products_10;


    use Illuminate\Database\Eloquent\Collection;
    use Mediapress\Foundation\AllBuilder\BuilderRenderable;
    use Mediapress\Modules\Content\Models\Page;

    class Pages extends BuilderRenderable
    {

        public function defaultContents()
        {

            extract($this->params);

            dd($this->params);
            //dd($page);

            return [
                [
                    "type" => "form",
                    "params" => [
                        "html" => [
                            "attributes" => [
                                "class" => "form-inline",
                                "method" => "GET",
                                "action" => url(route("admin.formbuilder.store"))
                            ]
                        ]
                    ],
                    "options" => [
                        "collectable_as" => ["form", "pagesform"]
                    ],
                    "contents" => [
                        [
                            "type" => "inputwithlabel",
                            "params" => [
                                "title" => "Page's Custom Varchar 1",
                                "html" => [
                                    "attributes" => [
                                        "name" => "page-><print>page->id</print>->cvar_1",
                                        "value" => "<print>page->cvar_1</print>",
                                    ]
                                ]
                            ]
                        ],
                        [
                            "type" => "detailtabs",
                            "params" => [
                                "details" => "<var>page->details</var>",
                            ],
                            "contents" => [

                                "tab" => [
                                    "type" => "tab",
                                    "params" => [
                                        "title" => "<print>detail->language->name</print>"
                                    ],
                                    "contents" => [
                                        [
                                            "type" => "inputwithlabel",
                                            "params" => [
                                                "title" => "Başlık",
                                                "html" => [
                                                    "attributes" => [
                                                        "name" => "detail->name",
                                                        "value" => "<print>detail->name</print>"
                                                    ]
                                                ]
                                            ]
                                        ],
                                        [
                                            "type" => "selectwithlabel",
                                            "params" => [
                                                "title" => "Kategori Seçin",
                                                "html" => [
                                                    "attributes" => [
                                                        "class" => "multiple",
                                                        "multiple" => "multiple",
                                                        "name"=>"page-><print>page->id</print>->extras->deneme45[]"
                                                    ]
                                                ]
                                            ],
                                            "options" => [
                                                "show_default_option" => false
                                            ],
                                            "data" => [
                                                "values" => [
                                                    "type" => "function_on_item",
                                                    "item" => "<var>page->details</var>",
                                                    "function" => "pluck",
                                                    "params" => [
                                                        "name",
                                                        "id"
                                                    ]
                                                ],
                                                "goon"=>[
                                                    "type"=>"function_on_item",
                                                    "item"=>[
                                                        "type"=>"function_on_item",
                                                        "item"=>$q,
                                                        "function"=>"where",
                                                        "params"=>["id",">",0]
                                                    ],
                                                    "function"=>"first",
                                                    "params"=>[]
                                                ]
                                            ]
                                        ],
                                        [
                                            "type" => "toggleswitch",
                                            "data" => [
                                                "on_value" => "dogru",
                                                "off_value" => "yanlis",
                                                "on_text" => "Doğru",
                                                "off_text" => "Yanlış"
                                            ]
                                        ],
                                        [
                                            "type"=>"blank",
                                            "params"=>[
                                                "html"=>["tag"=>"div", "attributes"=>["class"=>"clearfix"]]
                                            ]
                                        ]
                                    ]
                                ]

                            ]
                        ],
                        [
                            "type" => "tabs",
                            "contents" => [
                                "tab 1" => [
                                    "type" => "tab",
                                    "params" => [
                                        "title" => "Deneme Tab 1"
                                    ],
                                    "contents"=>[
                                        "div" => [
                                            "type"=> "blank",
                                            "params"=>[
                                                "html"=>[
                                                    "tag"=>"div"
                                                ]
                                            ]
                                        ]
                                    ]
                                ],
                                "tab 2" => [
                                    "type" => "tab",
                                    "params" => [
                                        "title" => "Deneme Tab 2"
                                    ],
                                    "contents"=>[
                                        "div" => [
                                            "type"=> "blank",
                                            "params"=>[
                                                "html"=>[
                                                    "tag"=>"div"
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        [
                            "type" => "button",
                            "params" => [
                                "html" => [
                                    "tag" => "button",
                                    "attributes" => [
                                        "type" => "submit"
                                    ]
                                ]
                            ],
                            "contents" => [
                                "Gönder"
                            ]
                        ]
                    ]
                ]
            ];

        }
    }