<?php

namespace Foundation\EDFormBuilder;

class EDFormBuilder
{
    protected $sections = [
        "default"=>null
    ];
    protected $form;

    public function __construct($object_class, $object_id)
    {
        switch($object_class){
            case "Mediapress\\Models\\Sitemap":
                break;
            default:
                break;
        }
        $this->form = new EDForm($object);
        $this->sections["default"] = new EDFormSection(null,null);
    }

    public function getVariables(){
        return ["default_website" => 5, "active_website" => 5];
    }

    public function createField($fieldClass,$params){
        return new $fieldClass($params);
    }
}
