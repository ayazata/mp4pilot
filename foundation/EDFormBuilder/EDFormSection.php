<?php
    /**
     * Created by PhpStorm.
     * User: eraye
     * Date: 17.09.2018
     * Time: 03:37
     */

    namespace Foundation\EDFormBuilder;


    class EDFormSection
    {
        public $language_code = null;
        public $country_code = null;
        public $section_code = null;

        public $fields=[];

        public function __construct($language_code, $country_code){
            $this->language_code = $language_code;
            $this->country_code = $country_code;
            $this->section_code = ($language_code . $country_code) ? ($language_code . $country_code) : "default";
        }

        public function add_field(EDFormField $field){
            $fields[$field->name] = $field;
        }

        public function render(){
            dump($this);
        }
    }
